<?php

function token($length = 32)
{

	// Create random token

	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';



	$max = strlen($string) - 1;



	$token = '';



	for ($i = 0; $i < $length; $i++) {

		$token .= $string[mt_rand(0, $max)];
	}



	return $token;
}



/**

 * Backwards support for timing safe hash string comparisons

 *

 * http://php.net/manual/en/function.hash-equals.php

 */



if (!function_exists('hash_equals')) {

	function hash_equals($known_string, $user_string)
	{

		$known_string = (string) $known_string;

		$user_string = (string) $user_string;



		if (strlen($known_string) != strlen($user_string)) {

			return false;
		} else {

			$res = $known_string ^ $user_string;

			$ret = 0;



			for ($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);



			return !$ret;
		}
	}
}

if (!function_exists('PostSmsRequest')) {
	function smsRequest($_data)
	{
		$url = "http://bulksms.altuminfotech.com/vendorsms/pushsms.aspx";

		$data = array(
			'user' => "genericure",
			'password' => "Genericure@Msg",
			'msisdn' =>'91'.((isset($_data['phone']))? $_data['phone']:"9881815256"),
			'sid' => "GNCURE",
			'msg' => (isset($_data['msg']))? $_data['msg']:"Test Message from API",
			'fl' => "0",
			'gwid' => "2",
		);

		$method = 'GET';
		return Call_API_SMS($method, $url, $data);
	}
}

if (!function_exists('logit')) {
 	function logit($log='', $filename='')
    {
		$filename = (empty($filename))? 'log_'.date("j.n.Y").'_'.$_SERVER['REMOTE_ADDR'].'.log': $filename.'_'.$_SERVER['REMOTE_ADDR'].'.log';


		$log = PHP_EOL.'----------------START ------------------'.PHP_EOL.$log;
		$log .= 'REQUEST:'.PHP_EOL;
		$log .= 'METHOD:'.$_SERVER['REQUEST_METHOD'].PHP_EOL;
		$log .= 'REQUEST_URI:'.$_SERVER['REQUEST_URI'].PHP_EOL;
		$log .= 'PARMS:'.serialize(array_merge($_POST,$_GET)).PHP_EOL;
		//$log .= '_SERVER:'.print_r($_SERVER, true);
		$log .= 'OUTPUT';
		//$log .= serialize($this->json).PHP_EOL;

		$log .= '---------------- END ------------------'.PHP_EOL;

		//Save string to log, use FILE_APPEND to append.
		file_put_contents('./logs/'.$filename, $log, FILE_APPEND);

	}
}

if (!function_exists('Call_API_SMS')) {
	function Call_API_SMS($method, $url, $data)
	{
		$url = $url;
		$curl = curl_init();
		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		// Optional Authentication:
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, "demo:demo");
		$headers = array(
			'Content-Type: application/json',
			sprintf('Authorization: Bearer %s', 'ssssssssssssssssssssssssss')
		);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT,10);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($curl);
		//print_r($result);
		$log = " sms_result : " .print_r($data,true). print_r($result, true) . " \n \n ";
		logit($log, 'Call_API_SMS');
		curl_close($curl);

		return $result;
	}
	
}


if (!function_exists('Call_REST_API')) {
	function Call_REST_API($method='GET', $url='', $data=array())
	{
		$url = 'http://192.168.1.139/GIT/generikart.com/api/rest/'.$url;
		$curl = curl_init();
		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		// Optional Authentication:
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, "demo:demo");
		$headers = array(
			'X-Oc-Merchant-Id: Y2aGbg16U1cRzzUNIvacbukVPZ0itzXM',
			'X-Oc-Session: 804c66e30a7cece656ca14ef7c',
			'X-Oc-Merchant-Language: en-gb'
			
			
		);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_TIMEOUT,10);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($curl);
		//print_r($result);
		$log = " sms_result : " .print_r($data,true). print_r($result, true) . " \n \n ";
		logit($log, 'Call_API_SMS');
		curl_close($curl);

		return json_decode($result, true);
	}
}