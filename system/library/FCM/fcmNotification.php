<?php
use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Topic;
use paragraph1\phpFCM\Notification;

require_once 'vendor/autoload.php';

class fcmNotification{
    public static function  send($customer_fcmid='',$parms=[]){
        $apiKey = 'AAAA1EB_IS0:APA91bHfolgf9mikRqijk5wkVrnFesyajb0aIoJgLA3AsPvjOaGGIUxZiksDJjwNzSMK94ddkdUrSDCph_lPmVvWNmooKg1AhEc58xysrAeRP_imSuTJCAqyQU_mbeG2fHD4AGqTdx5h';
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->addRecipient(new Topic('general'));
        //select devices where has 'your-topic1' && 'your-topic2' topics
        //$message->addRecipient(new Topic(['your-topic1', 'your-topic2']));

        $title = (isset($parms['title']))?$parms['title']: 'test title';
        $msg = (isset($parms['msg']))?$parms['msg']: 'testing body';
        $message->setNotification(new Notification($title, $msg))
            ->setData(array('someId' => 111));

        $response = $client->send($message);
        //var_dump($response);
        // Store JSON data in a PHP variable
        $json = [
            'status_code' => $response
        ];

    return   json_encode($json);
    }
}
