Custom payment method Module details please read instructions carefully:

There is two zip inside opencartpaymentmethod folder
 (a).CustomPayment.zip
 (b).RefundandStatus.zip

Instructions:

 With the help of CustomPayment.zip custom payment method will show in admin
 and you can alos enable/disable module

 1. Upload zip file on your server root.
 2. For view extention :
    Admin->Extensions->Extensions->Choose the extension type(Payment)
    after page load you can view CustomPayment
 3. Enable/Disable Method.
 4. Configure admin settings as your Merchant(2party Or 3Party)
 5. Please don't forgot Merchant Type checkbox as your Merchant.


  Before uploading RefundandStatus.zip, Please check your current theme name
  upload direcories as your current theme
  Note: RefundandStatus.zip work as per default and core admin files

 1. Upload zip file on your server root.
 2. For view extention :
    Admin->Sales->Orders
 3. If order status is Complete,Refunded and Refund in process then Refund tab
    is showing in order.
 4. Status tab is show for every order.
 5. Please read README.txt file carefully.



Important setting like (field in database and order status in opencart admin)
------------------------------------------------------------------------------------

For add order status Refund in process:

Goto admin->System->Localisation->Order Statuses->Add new order status

 Order Status Name = Refund In Process

 ----------------------------------------------------------------------------------

 Some SQl changes as below

 Find 'oc_order_history' table in your opencart database
 and add some extera field for custom payment method
 field name as below:
  	txn_ref_no
  	ret_ref_no
  	auth_code

------------------------------------------------------------------------------------


Module have 2 section Admin and Frontend
----------------------------------------------------------------------------------
 Module directiories:
 Admin(admin payment method configuration)
 Catalog(frontend payment method view)

 Admin:
 admin/controller/extension/payment/custompayment.php
 admin/language/en-gb/extension/payment/custompayment.php
 admin/view/template/extension/payment/custompayment.twig
 admin/view/image/payment/custompayment.png

 Frontend:
 catalog/controller/extension/payment/custompayment.php
 catalog/language/en-gb/extension/payment/custompayment.php
 catalog/model/extension/payment/custompayment.php
 catalog/view/theme/default/template/extension/payment/custompayment.twig
 catalog/view/theme/default/template/extension/payment/custom_two_party.twig
 catalog/view/theme/default/image/payment/custompayment.png
-----------------------------------------------------------------------------------
 if you want to Add Refund and Status tab for order then
 Modify below files as your theme


 Modify Admin order view section:
 admin/controller/sale/order.php
 admin/language/en-gb/sale/order.php
 admin/view/template/sale/order_info.twig

 Modify forntend order history section:
 catalog/controller/account/order.php
 catalog/language/en-gb/account/order.php
 catalog/view/theme/default/template/account/order_list.twig





git config git-ftp.url ftp://genericure.in
git config git-ftp.user ftp_username
git config git-ftp.password ftp_password