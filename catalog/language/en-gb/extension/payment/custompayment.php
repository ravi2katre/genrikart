<?php
$_['text_title']  = 'Credit/Debit/NetBanking Payment';
$_['token_error'] ='Error to choose a payment method. Contact the store owner';
$_['text_order']  = 'Order #';
$_['img_title']			= '';
$_['text_wait']					= 'Validating your credit card...';
$_['text_credit_card']			= 'Card Details';

// Entry
$_['entry_cc_type']				= 'Card Type';
$_['entry_cc_number']			= 'Card Number';
$_['entry_cc_expire_date']		= 'Card Expiry Date';
$_['entry_cc_cvv2']				= 'CV Code';
$_['entry_cc_issue']			= 'Card Issue Number';

// Help
$_['help_start_date']			= '(if available)';
$_['help_issue']				= '(for Maestro and Solo cards only)';
$_['place_order']  = 'Place Order';

//validation error
$_['card_number']				= 'Card Number is Required';
$_['card_expiry_month']			= 'Card Expiry Month is Required';
$_['card_expiry_year']			= 'Card Expiry Year is Required';
$_['card_cvc']					= 'Card CVC is Required';
$_['bank_code']					= 'Please Choose Bank From Option';
$_['radio_wallet']				= 'Please Choose Wallet From Option';
?>
