<?php
// Text
$_['text_subject']          = '%s - Order %s';
$_['text_greeting']         = 'Thank you for your interest in %s products. Your order has been received and will be processed once payment has been confirmed.';
$_['text_greeting_prescription_is_uploaded'] = 'Thank you.Your prescription has been recieved.You will be updated shortly.';
$_['text_greeting_products_Prescription'] = 'Thank you.Your Order has been placed successfully.You will be updated shortly.';
$_['text_greeting_products_only']  = 'Thankyou for your interest in Genericure products.Your order has been placed.';

$_['text_link']             = 'To view your order click on the link below:';
$_['text_order_detail']     = 'Order Details';
$_['text_instruction']      = 'Instructions';
$_['text_order_id']         = 'Order ID:';
$_['text_date_added']       = 'Date Added:';
$_['text_order_status']     = 'Order Status:';
$_['text_payment_method']   = 'Payment Method:';
$_['text_shipping_method']  = 'Shipping Method:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Telephone:';
$_['text_ip']               = 'IP Address:';
$_['text_payment_address']  = 'Payment Address';
$_['text_shipping_address'] = 'Shipping Address';
$_['text_products']         = 'Products';
$_['text_product']          = 'Product';
$_['text_model']            = 'Model';
$_['text_quantity']         = 'Quantity';
$_['text_price']            = 'Price';
$_['text_order_total']      = 'Order Totals';
$_['text_total']            = 'Total';
$_['text_download']         = 'Once your payment has been confirmed you can click on the link below to access your downloadable products:';
$_['text_comment']          = 'The comments for your order are:';
$_['text_footer']           = 'Please reply to this e-mail if you have any questions.';