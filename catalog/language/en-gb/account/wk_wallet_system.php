<?php
// Heading
$_['heading_title']		= 'My Wallet';

// Entry
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';

// Text
$_['text_wallet']			       = 'Wallet';
$_['text_balance']			     = 'Wallet Balance:';
$_['text_add']				       = 'Add Amount';
$_['text_add_wallet']		     = 'Add money to wallet';
$_['text_credit']			       = 'Credit History';
$_['text_debit']			       = 'Debit History';
$_['text_order_id']			     = 'Order ID';
$_['text_description']		   = 'Description';
$_['text_amount_credited']	 = 'Amount Credited';
$_['text_amount_debited']	   = 'Amount Debited';
$_['text_status']			       = 'Transaction Status';
$_['text_date']				       = 'Date Added';
$_['text_my_wallet']		     = 'My Wallet';
$_['text_wk_wallet_system']  = 'Wallet System';
$_['text_complete']			     = 'Complete';
$_['text_pending']			     = 'Pending';
$_['text_load_more']		     = 'Load more';
$_['text_use_wallet']		     = 'Use Wallet';
$_['text_bank_details']		   = 'Bank Details';
$_['text_preffered_method']	 = 'Please select the preferred payment method to use on this order.';
$_['text_preffered_method1'] = '<div class="alert alert-info">Please select the preferred payment method to use for paying the remaining amount of this order.</div>';
$_['text_bank_details']		   = 'Bank Details';
$_['text_warning_limit']	   = 'Wallet recharge range must be between %s to %s';
$_['text_warning_bank']		   = 'Warning: Sorry, the details can not be updated due to some issue!';
$_['text_recharge']			     = 'Recharge';
$_['text_remaining']		     = 'Remaining Amount: ';
$_['text_success']			     = 'Success: Amount is successfully added to your wallet';
$_['text_success_add']		   = 'Success: The amount is successfully added to cart. You have to checkout for recharging your wallet.';
$_['text_bank_success']		   = 'Success: Your bank details are updated successfully';
$_['text_terms']			       = 'Terms and Conditions';
$_['text_method']			       = 'Please select a payment method';
$_['text_amount']			       = 'Please enter amount to proceed';
$_['text_wallet_balance']    = 'Wallet Balance: %s';
$_['text_cart_total']        = 'Cart Total: %s';
$_['button_close']		       = 'Close';
$_['button_bank_details']	   = 'Update Bank Details';

// Mail
$_['text_transaction_received']	= 'A sum of %s have been successfully added to your wallet!';
$_['text_transaction_deduct']	  = 'A sum of %s have been successfully deducted from your wallet!';
$_['text_current_balance']		  = 'Your current wallet balance is %s';
$_['text_transaction_subject']	= '%s - Wallet System';
$_['text_dear']					        = 'Dear %s,';
$_['text_regards']				      = 'Regards';

// Error
$_['error_cart_overwrite']	= 'Unable to add this product to cart as cart contains Wallet.';
$_['error_cart'] 			      = 'Please remove wallet from the cart or first checkout with wallet to add this product to the cart.';
$_['error_quantity']		    = 'You can not update wallet quantity. Please add the amount to wallet.';
$_['error_login']			      = 'You must login/register before adding money to your wallet.';
$_['error_cart_other']		  = 'The cart contains other products. You have to remove them for adding money to wallet.';
$_['error_store_wallet']    = 'You are not allowed to add amount in wallet on this store.';
