<?php
// Heading
$_['heading_title'] = 'Payee List';

//text
$_['entry_email']                = 'E-mail';

$_['text_add_payee']             = 'Add New Payee';
$_['text_payee_list']            = 'Payee List';
$_['text_payee_name']            = 'Payee Name';
$_['text_payee_email']           = 'Payee Email';
$_['text_payee_date_added']      = 'Date Added';
$_['text_action']                = 'Action';
$_['text_add_payee_first']       = 'You have to add payee first for transferring wallet money.';
$_['text_not_added_any_payee']   = 'You have not added any payee to your account yet';
$_['text_transfer_wallet_money'] = 'Transfer Wallet Money';
$_['text_amount']                = 'Amount';
$_['text_your_password']         = 'Your Password';
$_['text_confirm_delete']        = 'Are you sure want to delete %name% from the payee list';
$_['text_transfer_money']        = 'Transfer Money';
$_['text_transfer']              = 'Transfer';
$_['text_delete']                = 'Delete';
$_['text_confirm_payee']         = 'Confirm Payee';
$_['text_delete_payee_success']  = 'You have successfully deleted the payee!';
$_['text_delete_payee_error']    = 'You can not delete this payee';
$_['text_payee_already_exist']   = 'This payee already exist in your account';
$_['text_success_payee']         = 'You have successfully added a new payee';
$_['text_cannot_add_yourself']   = 'You can not add yourself as a payee!';
$_['text_provide_email']         = 'Please provide email';
$_['text_same_account']          = 'You can not transfer money to your account';
$_['text_insufficient_amount']   = 'You do not have sufficient amount to transfer';
$_['text_email_not_exist']       = 'E-mail does not exist';
$_['text_password_incorrect']    = 'Please enter the correct password!';
$_['text_enter_amount']          = 'Please enter the amount';
$_['text_click_here']            = 'Click Here';
$_['text_transfer_by']           = 'Transferred by %s';
$_['text_transfer_to']           = 'Transferred to %s';
$_['text_payee_not_exist']       = 'A payee with this e-mail does not exist';
$_['text_email_not_valid']       = 'Please enter the valid e-mail.';
$_['text_email_transfer']        = 'You have successfully transferred %s from your wallet to %s wallet.';
$_['text_email_credit']          = 'Your wallet account is credited by %s from %s account.';
$_['text_subject_debit']         = 'Your account debited';
$_['text_subject_credit']        = 'Your account credited';
$_['text_success_transfer']      = 'Money has been successfully transferred!';

$_['button_close']               = 'Close';
$_['button_add']                 = 'Add';
$_['button_confirm']             = 'Confirm';

$_['error_login']                = 'You are not logged-in. Please login to make use of this feature';
$_['error_transaction_number']   = 'Your transaction number limit has reached for this month! <a href="%s" target="_blank">Contact Us</a>';
$_['error_transaction_amount']   = 'Your transaction amount limit has reached for this month! <a href="%s" target="_blank">Contact Us</a>';
$_['error_transaction_number_r'] = '%s\'s transaction number limit has reached for this month! <a href="%s" target="_blank">Contact Us</a>';
$_['error_transaction_amount_r'] = '%s\'s transaction amount limit has reached for this month! <a href="%s" target="_blank">Contact Us</a>';
$_['error_limit_remaining']      = 'You can not transfer an amount greater than %s as your limit has been reached';
$_['error_limit_remaining_r']    = 'You can not transfer an amount greater than %s as %s\'s limit has been reached';
$_['error_not_number']           = 'You have not provided a valid amount!';
$_['error_amount_limit']         = 'Your transfer limit is in between %s - %s.';
