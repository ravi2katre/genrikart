<?php

class ControllerCommonCustom extends Controller {

	public function index() {

		$this->document->setTitle($this->config->get('config_meta_title'));

		$this->document->setDescription($this->config->get('config_meta_description'));

		$this->document->setKeywords($this->config->get('config_meta_keyword'));



		if (isset($this->request->get['route'])) {

			$this->document->addLink($this->config->get('config_url'), 'canonical');

		}



		$data['column_left'] = $this->load->controller('common/column_left');

		$data['column_right'] = $this->load->controller('common/column_right');

		$data['content_top'] = $this->load->controller('common/content_top');

		$data['content_bottom'] = $this->load->controller('common/content_bottom');

		$data['footer'] = $this->load->controller('common/footer');

		$data['header'] = $this->load->controller('common/header');



		$this->response->setOutput($this->load->view('common/home', $data));

	}

	function dont_have_priscription(){
		$json = $_REQUEST;
		$name = $this->request->post['name'];
		$mobile = $this->request->post['mobile'];	
		$request_email = $this->request->post['request_email'];		
		$cart_data = json_encode($this->cart->getProducts());
		$customer_id = 0;
		if ($this->customer->isLogged()) {
			$customer_id = $this->customer->getId();
		}
		$data   =   array(
			'name'   =>  $name,
			'mobile'  =>  $mobile,
		);
		$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_consultation_request 
		SET 
		name = '" . $this->db->escape($name) . "',
		customer_id = '" . $customer_id . "',
		cart_data = '" . $cart_data . "',		
		request_email = '" . $request_email . "',
		mobile = '" . $this->db->escape($mobile) ."'"	);
		
		$to = $request_email.",nikhilt613@gmail.com,ravi2katre@gmail.com";
		$from = '';
		$text = 'Thanks. You wil receive a call  from our doctor soon.'.PHP_EOL;
		$data['to_name'] = $name;
		$data['subject'] = "Receive call from our doctor for prescription";
		$products = json_decode($cart_data, true);
		
		

		$this->notify($to, $from, $text, $data);// notify customer

		$to = 'drsanket08@gmail.com,nikhilt613@gmail.com,ravi2katre@gmail.com';
		$from = '';
		$text = 'Request you to provide prescription for: '.PHP_EOL;
		$text .= 'Name: '.$name.PHP_EOL;
		$text .= 'Email: '.$request_email.PHP_EOL;
		$text .= 'Phone: '.$mobile.PHP_EOL;
		$data['to_name'] = 'Dr. Sanket Nakhale';
		$data['subject'] = "Request you to provide prescription";

		$ul_list = '';
		$ul_list .= PHP_EOL.'Products'.PHP_EOL;
		foreach($products as $key=>$val){
			$ul_list .= $val['name']." (".$val['product_id'].')'.PHP_EOL;
		}
		
		$text .= PHP_EOL.$ul_list;
		$this->notify($to, $from, $text, $data);// notify to doctor 
		$json['status'] = true;
		$json['data'] = array();
		$json['msg'] = 'Thanks. You will receive a call from our doctor soon.';

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	function notify($to='', $from='', $text='ssss', $data=array()){
		//$this->load->language('mail/order_alert');
		//$this->load->language->load('mail/order_edit');
		//$this->load->language('mail/order_edit');
		//$this->load->language('mail/order_alert');
		
		
		$this->load->model('setting/setting');
		$data['comment'] = $text;

		$this->load->model('setting/setting');	
	
		if (!$from) {
			$from = $this->config->get('config_email');
		}
		if (!$data['subject']) {
			$data['subject'] = 'subject not set';
		}
		
		$mail = new Mail($this->config->get('config_mail_engine'));
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($to);
		$mail->setFrom($from);
		$mail->setSender(html_entity_decode('Genericure', ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode(sprintf($data['subject'], 'Genericure'), ENT_QUOTES, 'UTF-8'));
		$mail->setText($this->load->view('mail/common_email', $data));
		$mail->send();
	}

}

