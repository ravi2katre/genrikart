<?php

/**
 * contact.php
 *
 * Contact message management
 *
 * @author     Opencart-api.com
 * @copyright  2017
 * @license    License.txt
 * @version    2.0
 * @link       https://opencart-api.com/product/shopping-cart-rest-api/
 * @documentations https://opencart-api.com/opencart-rest-api-documentations/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestCustom extends RestController
{

    private $error = array();

    public function send()
    {

        $this->checkPlugin();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            $postData = $this->getPost();

            $this->language->load('information/contact');

            if ($this->validate($postData)) {

                if ($this->opencartVersion <= 2011) {
                    $mail = new Mail($this->config->get('config_mail'));
                } else {
                    $mail = new Mail();
                    $mail->protocol = $this->config->get('config_mail_protocol');
                    $mail->parameter = $this->config->get('config_mail_parameter');

                    if ($this->opencartVersion == 2020) {
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_host');
                    } else {
                        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                    }

                    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                    $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                    $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                    $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
                }

                $mail->setTo($this->config->get('config_email'));
                $mail->setFrom($this->config->get('config_email'));
                $mail->setReplyTo($postData['email']);
                $mail->setSender(html_entity_decode($postData['name'], ENT_QUOTES, 'UTF-8'));
                $mail->setSubject(html_entity_decode(sprintf($this->language->get('email_subject'), $postData['name']), ENT_QUOTES, 'UTF-8'));
                $mail->setText($postData['enquiry']);
                $mail->send();

            } else {
                $this->statusCode = 200;
                $this->json['error'] = $this->error;
            }
        } else {
            $this->statusCode = 200;
            $this->allowedHeaders = array("POST");
        }

        $this->sendResponse();
    }

	function notify_pricription_customers(){

		$post = $this->getPost();
		$json = $_REQUEST;

		$specialsQuery = $this->db->query("SELECT  dcr.* FROM 
		" . DB_PREFIX . "doctor_consultation_request dcr WHERE 
		dcr.date >= '2020-03-18 00:00:00' AND dcr.date <= '2020-04-22 23:59:59'
		group by dcr.customer_id
		");
		$rows = $specialsQuery->rows;
		

		
		$from = '';
		$text = 'Greetings from Team Genericure. 
		We would like to inform you that courier services have been resumed & we have started processing orders. 
		So if you want to place order just order through Genericure  app or you can WhatsApp your order at 9767029908 or 8669999089.'.PHP_EOL;
		
		$text .= PHP_EOL.PHP_EOL."
		Thanks & Regards,
		Team Genericure." ;

		$data['subject'] = "Team Genericure";
		foreach ($rows as $row) {
			//print_r($row);
			$data['to_name'] = 'customer, ';
			$to = $row['request_email'];
			$this->notify($to, $from, $text, $data);// notify to customer 
		}
		
		$json['status'] = true;
		$json['data'] = array();
		$json['msg'] = 'Notifications send to customer successfully.';

       
        $this->json["success"] = 1;
		$this->json["data"] = $rows;
		$this->json['msg'] = $json['msg'];
        return $this->sendResponse();

	}
	/****************************************************************
     * Url: api/rest/dont_have_priscription
     *POST JSON {
            "request_email": "rkatre@tiuconsulting.com",
            "mobile": "9881815256",
            "name": "admin"
        }
     */

	function dont_have_priscription(){
        $post = $this->getPost();
		$json = $_REQUEST;
        $name = isset($post['name'])?$post['name']:'' ;
        if($name == ''){
            $this->json["error"][]  = "Name is empty";
        }
        $mobile = isset($post['mobile'])?$post['mobile']:'' ;
        if($mobile == ''){
            $this->json["error"][]  = "Mobile is empty";
        }
        $request_email =  isset($post['request_email'])?$post['request_email']:'' ;
        
        if($request_email == ''){
            $this->json["error"][]  = "Request Email is empty";
        }
        if(count($this->json["error"]) > 0){
            $this->json["success"] = 0;
            return $this->sendResponse();
        }

		$cart_data = json_encode($this->cart->getProducts());
		$customer_id = 0;
		if ($this->customer->isLogged()) {
			$customer_id = $this->customer->getId();
		}
		$data   =   array(
			'name'   =>  $name,
			'mobile'  =>  $mobile,
		);
		$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_consultation_request 
		SET 
		name = '" . $this->db->escape($name) . "',
		customer_id = '" . $customer_id . "',
		cart_data = '" . $cart_data . "',		
		request_email = '" . $request_email . "',
		mobile = '" . $this->db->escape($mobile) ."'"	);
		
		$to = $request_email.",nikhilt613@gmail.com,ravi2katre@gmail.com";
		$from = '';
		$text = 'Thanks. You wil receive a call  from our doctor soon.'.PHP_EOL;
		$data['to_name'] = $name;
		$data['subject'] = "Receive call from our doctor for prescription";
		$products = json_decode($cart_data, true);
		
		

		$this->notify($to, $from, $text, $data);// notify customer

		$to = 'drsanket08@gmail.com,nikhilt613@gmail.com,ravi2katre@gmail.com';
		$from = '';
		$text = 'Request you to provide prescription for: '.PHP_EOL;
		$text .= 'Name: '.$name.PHP_EOL;
		$text .= 'Email: '.$request_email.PHP_EOL;
		$text .= 'Phone: '.$mobile.PHP_EOL;
		$data['to_name'] = 'Dr. Sanket Nakhale';
		$data['subject'] = "Request you to provide prescription";

		$ul_list = '';
		$ul_list .= PHP_EOL.'Products'.PHP_EOL;
		foreach($products as $key=>$val){
			$ul_list .= $val['name']." (".$val['product_id'].')'.PHP_EOL;
		}
		
		$text .= PHP_EOL.$ul_list;
		$this->notify($to, $from, $text, $data);// notify to doctor 
		$json['status'] = true;
		$json['data'] = array();
		$json['msg'] = 'Thanks. You will receive a call from our doctor soon.';

       
        $this->json["success"] = 1;
        $this->json["data"]['msg'] = $json['msg'];
        return $this->sendResponse();
	}

	function notify($to='', $from='', $text='ssss', $data=array()){
		//$this->load->language('mail/order_alert');
		//$this->load->language->load('mail/order_edit');
		//$this->load->language('mail/order_edit');
		//$this->load->language('mail/order_alert');
		
		
		$this->load->model('setting/setting');
		$data['comment'] = $text;

		$this->load->model('setting/setting');	
	
		if (!$from) {
			$from = $this->config->get('config_email');
		}
		if (!$data['subject']) {
			$data['subject'] = 'subject not set';
		}
		
		$mail = new Mail($this->config->get('config_mail_engine'));
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($to);
		$mail->setFrom($from);
		$mail->setSender(html_entity_decode('Genericure', ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode(sprintf($data['subject'], 'Genericure'), ENT_QUOTES, 'UTF-8'));
		$mail->setText($this->load->view('mail/common_email', $data));
		$mail->send();
	}
    
}