<?php
class ControllerExtensionPaymentWkwalletsystempayment extends Controller {
	public function index() {
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');

		return $this->load->view('extension/payment/wk_wallet_system', $data);
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'wk_wallet_system_payment') {
			$this->load->model('account/wk_wallet_system');

			$this->model_account_wk_wallet_system->sendDeductMail();
			$this->model_account_wk_wallet_system->addOrderHistory($this->session->data['order_id']);

			$this->load->model('checkout/order');

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('wk_wallet_system_order_status_id'));
		}
	}
}
