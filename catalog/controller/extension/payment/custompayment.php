<?php
class ControllerExtensionPaymentCustomPayment extends Controller {
  //const API_VERSION = 2;

  public function index() {
    $this->language->load('extension/payment/custompayment');

    $this->load->model('checkout/order');
    $data['order_id'] = $this->session->data['order_id'];
    $orderidunique = time();

    $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
    $total = $order_info['total'];

    // order amount
    $cartTotal = $total;
    $amount = $cartTotal*100;

    $data['action'] = $this->config->get('payment_custompayment_GatewayTestURL');

    //$data['EncData'] = $this->config->get('payment_custompayment_EncData');
    $data['action'] =  $this->config->get('payment_custompayment_GatewayTestURL');
    $data['MerchantID'] = $this->config->get('payment_custompayment_MerchantID');
    $data['BankId'] = $this->config->get('payment_custompayment_BankId');
    $data['TerminalId'] = $this->config->get('payment_custompayment_TerminalId');
    $data['Version'] = $this->config->get('payment_custompayment_Version');

    $data['ReturnURL'] = $this->config->get('payment_custompayment_ReturnURL');
    $data['PassCode'] = $this->config->get('payment_custompayment_AccessCode');
    $data['MCC'] = $this->config->get('payment_custompayment_MCC');
    $data['Currency'] = $this->config->get('payment_custompayment_CurrencyCode');
    $data['TxnType'] = $this->config->get('payment_custompayment_TransactionType');
    $data['SecureKey'] = $this->config->get('payment_custompayment_SecureKey');
    $data['EncryptionKey']=$this->config->get('payment_custompayment_EncryptionKey');

    $pm_type = $this->config->get('payment_custompayment_merchant_type');
    if ($pm_type['twoparty'] == 1) {


    return $this->load->view('extension/payment/custom_two_party', $data);


    }else{

      $payload = array(
                        "Amount"                => $amount,
                        "ReturnURL"             => $data['ReturnURL'],
                        "Version"               => $data['Version'],
                        "PassCode"              => $data['PassCode'],
                        "BankId"                => $data['BankId'],
                        "TerminalId"            => $data['TerminalId'],
                        "MerchantId"            => $data['MerchantID'],
                        "MCC"                   => $data['MCC'],
                        "Currency"              => $data['Currency'],
                        "TxnType"               => $data['TxnType'],
                        "TxnRefNo"              => "opencart".$orderidunique.$data['order_id'],

                      );

      //------ remove null values
      $payload = array_filter($payload);
      ksort($payload);
      $utility = new Utility();
      $dataToPostToPG="";
      foreach ($payload as $key => $value){
        if("" == trim($value) && $value === NULL)
          {

            }
        else
          {
            $dataToPostToPG=$dataToPostToPG.$key."||".($value)."::";


            }
      }
        //Generate Secure hash on parameters
      $SecureHash = $this->generateSecurehash($payload, $data['SecureKey']);
        //Appending hash and data with ::
      $dataToPostToPG="SecureHash||".urlencode($SecureHash)."::".$dataToPostToPG;
      $dataToPostToPG=substr($dataToPostToPG, 0, -2);
        /*encrypting data with AES------START*/

      $EncKey = $data['EncryptionKey'];
      $EncData=$utility->encrypt($dataToPostToPG, $EncKey);

      //Removing last 2 characters (::)
      $data['EncData']=$EncData;
      //end of custom code

      $data['button_confirm'] = $this->language->get('button_confirm');
      $data['token'] = $this->generateToken();
      $data['token_error'] = $this->language->get('token_error');
      //$data['order_id'] = $this->session->data['order_id'];

       $oder_details = $this->load->model('checkout/order');
       $products = $this->model_checkout_order->getOrder($this->session->data['order_id']);
       // echo "<pre>";
       // print_r($products);
       // die();
      $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_custompayment_Version'), true);
      $json['redirect'] = $this->url->link('checkout/success');
      //end of order save

       return $this->load->view('extension/payment/custompayment', $data);
      //order save code
      $this->response->addHeader('Content-Type: application/json');
      $this->response->setOutput(json_encode($json));
      //return $this->load->view('extension/payment/custompayment', $data);
    }
  }

  public function generateSecurehash($sortedData, $securekey){
           $utility = new Utility();
           if($sortedData)
            {

              $SecureHash = $securekey;
                 foreach($sortedData as $key => $val)
                {
                      $SecureHash = $SecureHash . ($val);


                }
            }

            //Generate SHA-256 hash
              $SecureHash = hash('sha256', utf8_encode($SecureHash));
            return $SecureHash;
        }

  public function generateToken(){


    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
    $orderAmount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
    $orderAmount = (float)$orderAmount * pow(10,(int)$this->currency->getDecimalPlace($order_info['currency_code']));
    $orderAmount = intval(strval($orderAmount));

    $customer_array =  array (
      'address' => strlen($order_info['payment_address_1']) > 0 ? $order_info['payment_address_1'] : null,
      'first_name' => strlen($order_info['payment_firstname']) > 0 ? $order_info['payment_firstname'] : null,
      'last_name' => strlen($order_info['payment_lastname']) > 0 ? $order_info['payment_lastname'] : null,
      'country' => strlen($order_info['payment_iso_code_2']) > 0 ? $order_info['payment_iso_code_2'] : null,
      'city'=> strlen($order_info['payment_city']) > 0 ? $order_info['payment_city'] : null,
      'phone' => strlen($order_info['telephone']) > 0 ? $order_info['telephone'] : null,
      'email'=> strlen($order_info['email']) > 0 ? $order_info['email'] : null,
      'zip' => strlen($order_info['payment_postcode']) > 0 ? $order_info['payment_postcode'] : null,
      'ip' => $this->request->server['REMOTE_ADDR']
    );

    if (in_array($order_info['payment_iso_code_2'], array('US','CA'))) {
      $customer_array['state'] = $order_info['payment_zone_code'];
    }

    $order_array = array ( 'currency'=> $order_info['currency_code'],
      'amount' => $orderAmount,
      'description' => $this->language->get('text_order'). ' ' .$order_info['order_id'],
      'tracking_id' => $order_info['order_id']);

    // $callback_url = $this->url->link('extension/payment/custompayment/callback1', '', 'SSL');
    // $callback_url = str_replace('carts.local', 'webhook.custompayment.com:8443', $callback_url);

    // $setting_array = array ( 'success_url'=>$this->url->link('extension/payment/custompayment/callback', '', 'SSL'),
    //   'decline_url'=> $this->url->link('checkout/checkout', '', 'SSL'),
    //   'cancel_url'=> $this->url->link('checkout/checkout', '', 'SSL'),
    //   'fail_url'=>$this->url->link('checkout/checkout', '', 'SSL'),
    //   'language' => $this->_language($this->session->data['language']),
    //   'notification_url'=> $callback_url);

    // $transaction_type='payment';

    // $payment_methods_array = array(
    //   'types' => array(
    //   )
    // );

    $pm_type = $this->config->get('payment_custompayment_merchant_type');

    if ($pm_type['twoparty'] == 1) {
      $payment_methods_array['types'][] = 'two_party';
    }

    if ($pm_type['threeparty'] == 1) {
     $payment_methods_array['types'][] = 'three_party';
    }

    // if ($pm_type['erip'] == 1) {
    //   $payment_methods_array['types'][] = 'erip';
    //   $payment_methods_array['erip'] = array(
    //     'order_id' => $order_info['order_id'],
    //     'account_number' => $order_info['order_id'],
    //     'service_no' => $this->config->get('payment_custompayment_erip_service_no'),
    //     'service_info' => array($order_array['description'])
    //   );
    // }

    // $checkout_array = array(
    //   'version' => '2.1',
    //   'transaction_type' => $transaction_type,
    //   'settings' =>$setting_array,
    //   'order' => $order_array,
    //   'customer' => $customer_array,
    //   'payment_method' => $payment_methods_array
    //   );

    // $token_json =  array('checkout' =>$checkout_array );

    // $this->load->model('checkout/order');

    // $post_string = json_encode($token_json);

    // $username=$this->config->get('payment_custompayment_companyid');
    // $password=$this->config->get('payment_custompayment_encyptionkey');
    // $ctp_url = 'https://' . $this->config->get('payment_custompayment_domain_payment_page') . '/ctp/api/checkouts';

    // $curl = curl_init($ctp_url);
    // curl_setopt($curl, CURLOPT_PORT, 443);
    // curl_setopt($curl, CURLOPT_HEADER, 0);
    // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    //   'Content-Type: application/json',
    //   'Content-Length: '.strlen($post_string))) ;
    // curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
    // curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
    // curl_setopt($curl, CURLOPT_POST, 1);
    // curl_setopt($curl, CURLOPT_USERPWD, "$username:$password");
    // curl_setopt($curl, CURLOPT_POSTFIELDS, $post_string);

    // $response = curl_exec($curl);
    // curl_close($curl);

    // if (!$response) {
    //   $this->log->write('Payment token request failed: ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
    //   return false;
    // }

    // $token = json_decode($response,true);

    // if ($token == NULL) {
    //   $this->log->write("Payment token response parse error: $response");
    //   return false;
    // }

    // if (isset($token['errors'])) {
    //   $this->log->write("Payment token request validation errors: $response");
    //   return false;
    // }

    // if (isset($token['response']) && isset($token['response']['message'])) {
    //   $this->log->write("Payment token request error: $response");
    //   return false;
    // }

    // if (isset($token['checkout']) && isset($token['checkout']['token'])) {
    //   return $token['checkout']['token'];
    // } else {
    //   $this->log->write("No payment token in response: $response");
    //   return false;
    // }
  }

  public function confirm() {
    $json = array();
   // echo "hi";
    //success page url https://isgpayopencart.example.com/upload/index.php?route=extension/payment/custompayment/confirm
   // echo "<pre>";
   // print_r($_POST);
    $utility = new Utility();
    $EncKey = $data['EncryptionKey']=$this->config->get('payment_custompayment_EncryptionKey');
    $SECURE_SECRET = $data['SecureKey'] = $this->config->get('payment_custompayment_SecureKey');
    $EncData=trim(isset($_POST['EncData'])? $_POST['EncData'] : '');
    $merchantId = trim(isset($_POST['MerchantId'])?  $_POST['MerchantId'] : '');
    $bankID  = trim(isset($_POST['BankId']) ? $_POST['BankId'] : '');
    $terminalId =  trim(isset($_POST['TerminalId']) ? $_POST['TerminalId'] : '');

    if($bankID=="" && $merchantId=="" && $terminalId=="" && $EncData=="" )
    {
      echo "Invalid data";
      exit();
    }
    if(empty($bankID) && empty($merchantId) && empty($terminalId) && empty($EncData) )
    {
      echo "Invalid data";
      exit();
    }

    $data= $utility->decrypt($EncData, $EncKey);
    $data=substr($data, 0, -2);
    $dataArray=explode("::",$data);

    foreach ($dataArray as $key => $value)
    {
      $valuesplit=explode("||",$value);
      $dataFromPostFromPG[$valuesplit[0]]=urldecode($valuesplit[1]);
    }

    // SecureHash got in reply
    $SecureHash=$dataFromPostFromPG['SecureHash'];

    //remove SecureHash from data
    unset($dataFromPostFromPG['SecureHash']);

    //remove null or empty data
    unset($dataFromPostFromPG['']);

    //remove null or empty data
    array_filter($dataFromPostFromPG);

    //sort data array
    ksort($dataFromPostFromPG);

    $txnRefNo= $utility->null2unknown("TxnRefNo",$dataFromPostFromPG);

    $merchantId= $utility->null2unknown("MerchantId",$dataFromPostFromPG);
    $amount= $utility->null2unknown("Amount",$dataFromPostFromPG);
    $terminalId= $utility->null2unknown("TerminalId",$dataFromPostFromPG);
    $responseCode= $utility->null2unknown("ResponseCode",$dataFromPostFromPG);
    $message= $utility->null2unknown("Message",$dataFromPostFromPG);
    $retRefNo = $utility->null2unknown("RetRefNo",$dataFromPostFromPG);
    $batchNo= $utility->null2unknown("BatchNo",$dataFromPostFromPG);
    $authCode= $utility->null2unknown("AuthCode",$dataFromPostFromPG);
    $bankID= $utility->null2unknown("BankId",$dataFromPostFromPG);
    $issuerRefNo= $utility->null2unknown("issuerRefNo",$dataFromPostFromPG);
    $firstName= $utility->null2unknown("firstName",$dataFromPostFromPG);
    $lastName= $utility->null2unknown("lastName",$dataFromPostFromPG);
    $addressZip= $utility->null2unknown("addressZip",$dataFromPostFromPG);
    $pgTxnId= $utility->null2unknown("pgTxnId",$dataFromPostFromPG);

    $SecureHash_final = $this->generateSecurehash($dataFromPostFromPG, $SECURE_SECRET);

    $hashValidated = 'Invalid Hash';
    if( $SecureHash_final == $SecureHash )
    {
      $hashValidated = 'CORRECT';
    }
    $order_id = $this->session->data['order_id'];
    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($order_id);
    if ($order_info) {

    //$responseCode = "00";
      $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
      if(isset($responseCode) && $responseCode == '00'){
        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_completed_status_id'), $message, true);

         // insert transtion ref no,return ref no, auth code in opencart db
         $this->db->query("UPDATE `oc_order_history` SET `txn_ref_no` = '".$txnRefNo."' WHERE `order_id` = '".$order_id."'");
         $this->db->query("UPDATE `oc_order_history` SET `ret_ref_no` = '".$retRefNo."' WHERE `order_id` = '".$order_id."'");
         $this->db->query("UPDATE `oc_order_history` SET `auth_code` = '".$authCode."' WHERE `order_id` = '".$order_id."'");

        $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
      }
      elseif(isset($responseCode) && ($responseCode == '3DSF')){
        $this->cart->clear();
        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_failed_status_id'), $message, true);
        $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));

      }elseif(isset($responseCode) && ($responseCode == 'VER')){
         $this->cart->clear();
         $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_failed_status_id'), $message, true);
        $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));

      }elseif(isset($responseCode) && ($responseCode == 'CAN')){
        $this->cart->clear();
        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_cancel_status_id'), $message, true);
        $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));
      }elseif(isset($responseCode) && ($responseCode == 'HNM')){
        $this->cart->clear();
        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_cancel_status_id'), $message, true);
        $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));
      }else{
        $this->cart->clear();
        $this->response->redirect($this->url->link('checkout/failure', '', 'SSL'));
      }
    }








    // if($hashValidated == 'CORRECT' && $responseCode == '00')
    // {
    //   //Write your sucess transaction code for ex: Notification to customer, updating order/payment status, updating Inventory etc
    //   //Show success message to customer
    //   echo "<h4>Your Transaction ID for Order $txnRefNo  is ".$pgTxnId .".</h4>";
    //     echo "<h4>We have received a payment of Rs. " . $amount/100 . ". Your order will soon be shipped.</h4>";
    // }
    // else if($hashValidated == 'CORRECT' && $responseCode == 'CAN')
    // {
    //   //Write your sucess transaction code for ex: Notification to customer, updating order/payment status, updating Inventory etc
    //   //Show success message to customer
    //   echo "<h4>Your Transaction ID for Order $txnRefNo  is ".$pgTxnId .".</h4>";
    //     echo "<h4>Your order is cancelled. $message</h4>";
    // }
    // else
    // {
    //   //Write your sucess transaction code for ex: Notification to customer, updating order/payment status, updating Inventory etc
    //   //Show failure message to customer
    //   echo "<h4>Your Transaction ID for Order $txnRefNo  is ".$pgTxnId .".</h4>";
    //     echo "<h4>Your payment is not processed. $message</h4>";
    // }

    // echo "<BR><h4>Complete Response from PG For reference :<h4>";
    // print "<pre>";
    // print_r($dataFromPostFromPG);


    //die;


  }



  public function callback() {

    if (isset($this->session->data['order_id'])) {
      $order_id = $this->session->data['order_id'];
    } else {
      $order_id = 0;
    }

    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($order_id);

    $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
  }

  public function send() {
    $json = array();

    //$this->load->library('stripe');
    $this->load->model('checkout/order');
    $this->load->model('account/customer');
    $this->load->model('extension/payment/custompayment');

    //$stripe_environment = $this->config->get('payment_stripe_environment');
    $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
     //from post data for cc
      $cardNumber = isset($_POST['cardNumber']) ? $_POST['cardNumber'] : '';
      // remoce dash from credit card number
      $ccn_w_dash = str_replace('-', '', $cardNumber);

     //credit,debit card data
     $cardexpiremonth = isset($_POST['cardexpiremonth']) ? $_POST['cardexpiremonth'] : '';
     $cardexpiryyear = isset($_POST['cardexpiryyear']) ? $_POST['cardexpiryyear'] : '';
     $ExpiryDate = $cardexpiremonth.$cardexpiryyear;
     $cardcvc = isset($_POST['cardcvc']) ? $_POST['cardcvc'] : '';
     //netbanking data
     $bankCode = isset($_POST['bankcode']) ? $_POST['bankcode'] : '';
     //wallet data
     $radioWallet = isset($_POST['radiowallet']) ? $_POST['radiowallet'] : '';





     $payOpt = isset($_POST['optionvalue']) ? $_POST['optionvalue'] : '';

     //order infromation
     $amount =  round($order_info['total'] * 100);
     $orderId = $this->session->data['order_id'];

     //order info by order id
     $orderData = $this->model_checkout_order->getOrder($orderId);

     $FirstName = $orderData['firstname'];
     $LastName  = $orderData['lastname'];
     $Email     = $orderData['email'];
     $Phone     = $orderData['telephone'];
     $Street    = $orderData['payment_address_1'];
     $City      = $orderData['payment_city'];
     $ZIP       = $orderData['payment_zone_id'];
     $State     = $orderData['payment_zone'];

     //end of order info

     // configure data
     $data['place_order'] = $this->language->get('place_order');
     $data['action'] =  $this->config->get('payment_custompayment_GatewayTestURL');
    $data['MerchantID'] = $this->config->get('payment_custompayment_MerchantID');
    $data['BankId'] = $this->config->get('payment_custompayment_BankId');
    $data['TerminalId'] = $this->config->get('payment_custompayment_TerminalId');
    $data['Version'] = $this->config->get('payment_custompayment_Version');

    $data['ReturnURL'] = $this->config->get('payment_custompayment_ReturnURL');
    $data['PassCode'] = $this->config->get('payment_custompayment_AccessCode');
    $data['MCC'] = $this->config->get('payment_custompayment_MCC');
    $data['Currency'] = $this->config->get('payment_custompayment_CurrencyCode');
    $data['TxnType'] = $this->config->get('payment_custompayment_TransactionType');
    $data['SecureKey'] = $this->config->get('payment_custompayment_SecureKey');
    $data['EncryptionKey']=$this->config->get('payment_custompayment_EncryptionKey');
    //end of configure data

       $payload = array(
                       "Amount"                => $amount,
                       "ReturnURL"             => $data['ReturnURL'],
                       "Version"               => $data['Version'],
                       "PassCode"              => $data['PassCode'],
                       "BankId"                => $data['BankId'],
                       "TerminalId"            => $data['TerminalId'],
                       "MerchantId"            => $data['MerchantID'],
                       "MCC"                   => $data['MCC'],
                       "Currency"              => $data['Currency'],
                       "TxnType"               => $data['TxnType'],
                       "TxnRefNo"              => "opencart-".$orderId,
                       "CardNumber"            => $ccn_w_dash,
                       "ExpiryDate"            => $ExpiryDate,
                       "CardSecurityCode"      => $cardcvc,
                       "bankCode"              => $bankCode,
                       "radio-wallet"          => $radioWallet,
                       "payOpt"                => $payOpt,
                       "Phone"                 => $Phone,
                       "Email"                 => $Email,
                       "FirstName"             => $FirstName,
                       "LastName"              => $LastName,
                       "Street"                => $Street,
                       "City"                  => $City,
                       "ZIP"                   => $ZIP,
                       "State"                 => $State,

                      );

      //------ remove null values
      $payload = array_filter($payload);
      ksort($payload);
      $utility = new Utility();
      $dataToPostToPG="";
      foreach ($payload as $key => $value){
        if("" == trim($value) && $value === NULL)
          {

            }
        else
          {
            $dataToPostToPG=$dataToPostToPG.$key."||".($value)."::";


            }
       }
        //Generate Secure hash on parameters
      $SecureHash = $this->generateSecurehash($payload, $data['SecureKey']);
        //Appending hash and data with ::
      $dataToPostToPG="SecureHash||".urlencode($SecureHash)."::".$dataToPostToPG;
      $dataToPostToPG=substr($dataToPostToPG, 0, -2);
        /*encrypting data with AES------START*/
      $EncKey = $data['EncryptionKey'];
      $EncData=$utility->encrypt($dataToPostToPG, $EncKey);
      //Removing last 2 characters (::)
      $json['EncData']=$EncData;
      // addOrderHistory
      $json['success'] = $this->url->link('checkout/success');
       $this->response->addHeader('Content-Type: application/json');
       $this->response->setOutput(json_encode($json));
  }

  public function callback1() {

    $postData =  (string)file_get_contents("php://input");
    $post_array = json_decode($postData, true);
    $order_id = $post_array['transaction']['tracking_id'];
    $order_id = $post_array['transaction']['tracking_id'];
    $status = $post_array['transaction']['status'];
    $transaction_id = $post_array['transaction']['uid'];
    $transaction_message = $post_array['transaction']['message'];
    $three_d = $post_array['transaction']['three_d_secure_verification']['pa_status'];
    if (isset($three_d)) {
      $three_d = '3-D Secure: ' . $three_d . '.';
    } else {
      $three_d = '';
    }

    $this->log->write("Webhook received: $postData");
    $this->load->model('checkout/order');
    $order_info = $this->model_checkout_order->getOrder($order_id);
    if ($order_info) {
      $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('config_order_status_id'));
      if(isset($status) && $status == 'successful'){
        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_completed_status_id'), "UID: $transaction_id. $three_d Processor message: $transaction_message", true);
      }
      if(isset($status) && ($status == 'failed')){
        $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_custompayment_failed_status_id'), "UID: $transaction_id. Fail reason: $transaction_message", true);
      }
    }
  }

  private function _language($lang_id) {
    $lang = substr($lang_id, 0, 2);
    $lang = strtolower($lang);
    return $lang;
  }
}

class Utility
            {

              function encrypt($input, $key)
              {

                  return  base64_encode(openssl_encrypt($input,"AES-256-ECB", $key, OPENSSL_RAW_DATA ));
              }

              function decrypt($sStr, $key)
              {
                return  openssl_decrypt(base64_decode($sStr), 'AES-256-ECB', $key, OPENSSL_RAW_DATA);
              }

              function null2unknown($check_null,$Array_data)
              {
                if(!isset($Array_data[$check_null]))
                {
                  return "No Value Returned";
                }
                else
                {
                  return $Array_data[$check_null];
                }
              }
            }
