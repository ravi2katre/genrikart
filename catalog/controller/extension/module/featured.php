<?php
class ControllerExtensionModuleFeatured extends Controller
{
    public function index($setting)
    {$data['theme_path'] = $this->config->get('theme_path');
        $data['site_url'] = $this->config->get('site_url');
        $this->load->language('extension/module/featured');

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $this->load->model('tool/image');

        $data['products'] = array();

        if (!$setting['limit']) {
            $setting['limit'] = 4;
        }

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int) $setting['limit']);

            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                if ($product_info) {
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float) $product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float) $product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }

                    $data['products'][] = array(
                        'product_id' => $product_info['product_id'],
                        'thumb' => $image,
                        'name' => $product_info['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price' => $price,
                        'special' => $special,
                        'tax' => $tax,
                        'rating' => $rating,
                        'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    );
                }
            }
        }

        $display_categories = $this->model_catalog_category->getDisplayCategories('71,72,73,74,75,77,78,82');
		
        foreach ($display_categories as $key => $val) {
            $data['filter_category_id'] = $val['category_id'];
			$data['start'] = 1; 
			$data['limit'] = 20;
            $display_categories[$key]['products'] = $this->model_catalog_product->getProducts($data);
            
            if (count($display_categories[$key]['products']) > 0) {
                foreach ($display_categories[$key]['products'] as $key2 => $val2) {

                    $product_info = $this->model_catalog_product->getProduct($val2['product_id']);

                    if ($product_info) {
                        if ($product_info['image']) {
                            $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                        } else {
                            $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                        }

                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }

                        if ((float) $product_info['special']) {
                            $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $special = false;
                        }

                        if ($this->config->get('config_tax')) {
                            $tax = $this->currency->format((float) $product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                        } else {
                            $tax = false;
                        }

                        if ($this->config->get('config_review_status')) {
                            $rating = $product_info['rating'];
                        } else {
                            $rating = false;
                        }

                        $data2 = array(
                            'product_id' => $product_info['product_id'],
                            'thumb' => $image,
                            'name' => $product_info['name'],
                            'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                            'price' => $price,
							'special' => $special,
							'org_price' => $product_info['price'],
                            'org_special' => $product_info['special'],
                            'save_price' => $this->currency->format($product_info['price'] - $product_info['special'], $this->session->data['currency']),
                            'tax' => $tax,
                            'rating' => $rating,
                            'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        );

                        $display_categories[$key]['products'][$key2] = $data2;
                    }

                }
            }
        }
        $data['display_categories'] = $display_categories;
        //print_r($display_categories); exit;

        //if ($data['products']) {
            return $this->load->view('extension/module/featured', $data);
        //}
    }
}
