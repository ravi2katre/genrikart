<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
		$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
		$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		$data['module'] = $module++;		
		$this->load->model('extension/news');		$page = 1;		$filter_data = array(			'page' => $page,			'limit' => 10,			'start' => 10 * ($page - 1),		);		$data['all_news'] = array();				$all_news= $this->model_extension_news->getAllNews($filter_data);				foreach ($all_news as $news) {			$data['all_news'][] = array (				'image' => $this->model_tool_image->resize($news['image'], $setting['width'], $setting['height']),				'news_id'       => $news['news_id'],				'title' 		=> html_entity_decode($news['title'], ENT_QUOTES),				'short_description' 	=> (utf8_strlen(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))) > 50 ? utf8_substr(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES)), 0, 50) . '...' : strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))),				'view' 			=> $this->url->link('extension/news/news', 'news_id=' . $news['news_id']),				'date_added' 	=> date($this->language->get('date_format_short'), strtotime($news['date_added']))			);		}
		return $this->load->view('extension/module/carousel', $data);
	}
}