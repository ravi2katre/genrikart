<?php
class ControllerAccountWkwalletsystem extends Controller {

	public function index() {
		if (!$this->config->get('total_wk_wallet_system_status')) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/wk_wallet_system', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$data = $this->load->language('account/wk_wallet_system');

		$this->document->addScript('catalog/view/javascript/wk_wallet_system/classie.js');
		$this->document->addScript('catalog/view/javascript/wk_wallet_system/cbpFWTabs.js');
		$this->document->addScript('catalog/view/javascript/wk_wallet_system/jquery.stickyheader.js');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/demo1.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/set1.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/base.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/buttons.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/demo.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/tabs.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/tabstyles.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/demo2.css');
		$this->document->addStyle('catalog/view/theme/default/stylesheet/wk_wallet_system/component.css');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_wk_wallet_system'),
			'href' => $this->url->link('account/wk_wallet_system', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['warning'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('account/wk_wallet_system');

		$balance = $this->model_account_wk_wallet_system->walletBalance();
		$data['balance'] = $this->currency->format($balance, $this->session->data['currency'], 1);

		$bank_details = $this->model_account_wk_wallet_system->getBankDetails($this->customer->getId());

		$bank_indexes = array(
			'bank_name',
			'bank_branch_number',
			'bank_swift_code',
			'bank_account_name',
			'bank_account_number'
			);

		foreach ($bank_indexes as $bank_index) {
			if (isset($bank_details[$bank_index])) {
				$data[$bank_index] = $bank_details[$bank_index];
			} else {
				$data[$bank_index] = '';
			}
		}

		$data['credits'] = array();
		$data['debits'] = array();
		$credits = $this->model_account_wk_wallet_system->getCreditHistory();
		$debits = $this->model_account_wk_wallet_system->getDebitHistory();
		$data['credit_count'] = count($credits);
		$data['debit_count'] = count($debits);
		$data['total_credits'] = $this->model_account_wk_wallet_system->getTotalCreditHistory();
		$data['total_debits'] = $this->model_account_wk_wallet_system->getTotalDebitHistory();

		foreach ($credits as $credit) {
			if (empty($credit['currency_code'])) {
				$credit['currency_code'] = $this->config->get('config_currency');
			}

			if (!$credit['order_id']) {
				$credit['order_id'] = 'N/A';
			}
			$data['credits'][] = array(
				'order_id'	=> $credit['order_id'],
				'amount'	=> $this->currency->format(($this->currency->convert($credit['amount'], $credit['currency_code'], $this->session->data['currency'])), $this->session->data['currency'], 1),
				'status'		=> $credit['status'] ? $this->language->get('text_complete') : $this->language->get('text_pending'),
				'date'		=> $credit['date'],
				'desc'		=> $credit['description']
			);
		}

		foreach ($debits as $debit) {
			if (empty($debit['currency_code'])) {
				$debit['currency_code'] = $this->config->get('config_currency');
			}

			if (!$debit['order_id']) {
				$debit['order_id'] = 'N/A';
			}

			$data['debits'][] = array(
				'order_id' => $debit['order_id'],
				'amount'   => $this->currency->format(($this->currency->convert($debit['amount'], $debit['currency_code'], $this->session->data['currency'])), $this->session->data['currency'], 1),
				'date'     => $debit['date'],
				'desc'     => $debit['description']
			);
		}

		$terms = html_entity_decode($this->config->get('wk_wallet_system_terms'));
		$regex_for_script = '~<script[^>]*>(.*?(\n))*(.*?)</script>~';
		$data['data_terms'] = preg_replace($regex_for_script, "", $terms);

		if ($this->config->get('wk_wallet_system_min_recharge')) {
			$data['min_amount'] = $this->config->get('wk_wallet_system_min_recharge');
		} else {
			$data['min_amount'] = 0.01;
		}

		if ($this->config->get('wk_wallet_system_max_recharge')) {
			$data['max_amount'] = $this->config->get('wk_wallet_system_max_recharge');
		} else {
			$data['max_amount'] = 999999999;
		}

		$data['limit_warning'] = sprintf($this->language->get('text_warning_limit'), $data['min_amount'], $data['max_amount']);
		$data['bank_action'] = $this->url->link('account/wk_wallet_system/updateBankDetails', '', true);

		if(isset($this->request->get['transfer'])) {
			$data['debit_transfer'] = $this->request->get['transfer'];
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (version_compare(VERSION, '2.2.0.0', '>=')) {
			$this->response->setOutput($this->load->view('account/wk_wallet_system', $data));
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/wk_wallet_system.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/wk_wallet_system.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/account/wk_wallet_system.tpl', $data));
			}
		}
	}

	public function loadData() {
		$json['data'] = array();

		if (isset($this->request->post['type']) && isset($this->request->post['start'])) {
			$this->load->model('account/wk_wallet_system');
			$this->load->language('account/wk_wallet_system');

			if ($this->request->post['type'] == 1) {
				$filter_data['start'] = $this->request->post['start'] + 1;
				$data = $this->model_account_wk_wallet_system->getCreditHistory($filter_data);

				foreach ($data as $credit) {
					if (!$credit['order_id']) {
						$credit['order_id'] = 'N/A';
					}

					if (!$credit['currency_code']) {
						$credit['currency_code'] = $this->config->get('config_currency');
					}

					$json['data'][] = array(
						'order_id'	=> $credit['order_id'],
						'amount'	=> $this->currency->format($credit['amount'], $credit['currency_code'], 1),
						'status'		=> $credit['status'] ? $this->language->get('text_complete') : $this->language->get('text_pending'),
						'date'		=> $credit['date'],
						'desc'		=> $credit['description']
					);
				}
				$json['total'] = count($json['data']);
			} elseif ($this->request->post['type'] == 2) {
				$filter_data['start'] = $this->request->post['start'] + 1;
				$data = $this->model_account_wk_wallet_system->getDebitHistory($filter_data);

				foreach ($data as $debit) {
					if (!$debit['order_id']) {
						$debit['order_id'] = 'N/A';
					}

					if (!$debit['currency_code']) {
						$debit['currency_code'] = $this->config->get('config_currency');
					}

					$json['data'][] = array(
						'order_id'	=> $debit['order_id'],
						'amount'	=> $this->currency->format($debit['amount'], $debit['currency_code'], 1),
						'date'		=> $debit['date'],
						'desc'		=> $debit['description']
					);
				}
				$json['total'] = count($json['data']);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addMoney() {
		$this->load->language('account/wk_wallet_system');
		$json = array();

		if (!$this->customer->isLogged()) {
			$json['error'] = $this->language->get('error_login');
		}

		if (!$json && is_numeric($this->request->post['amount'])) {
			$has_product = $this->cart->hasProducts();
			if ($has_product) {
				if ($has_product > 1) {
					$json['error'] = $this->language->get('error_cart_other');
				} else {
					$products = $this->cart->getProducts();
					if ($products[0]['product_id'] == $this->config->get('wallet_product_id')) {
						$amount = $this->currency->convert($this->request->post['amount'], $this->session->data['currency'], $this->config->get('config_currency'));

						$this->session->data['wk_wallet_amount'] += $amount;

						$this->cart->remove($products[0]['cart_id']);
					} else {
						$json['error'] = $this->language->get('error_cart_other');
					}
				}
			} else {
				$amount = $this->currency->convert($this->request->post['amount'], $this->session->data['currency'], $this->config->get('config_currency'));

				$this->session->data['wk_wallet_amount'] = $amount;
			}

			if (!$json) {
				$this->request->post['product_id'] = $this->config->get('wallet_product_id');
				$json = $this->load->controller('checkout/cart/add');
				if (isset($json['total'])) {
					$json['success'] = $this->language->get('text_success_add');
				} else {
					$json['error'] = $this->language->get('error_store_wallet');
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function transferMoney() {
		$json = array();
		$this->load->language('account/wk_wallet_payee');
		$this->load->model('account/wk_wallet_system');
		$this->load->model('account/customer');

		$password = $this->model_account_wk_wallet_system->checkPassword($this->request->post['password']);

		if($password) {
			$email_info = $this->model_account_customer->getCustomerByEmail($this->request->post['receiver_email']);

			if ($email_info) {
				$pay_amount = $this->request->post['payee_amount'];
				$balance = $this->model_account_wk_wallet_system->walletBalance();
				$sender_transactions = $this->model_account_wk_wallet_system->sentMonthlyTransactions($this->customer->getId());
				$receiver_transactions = $this->model_account_wk_wallet_system->receivedMonthlyTransactions($email_info['customer_id']);
				$receiver_name = $email_info['firstname'] . ' ' . $email_info['lastname'];

				$receiver_transact_total = $receiver_transactions['total'];
				$receiver_transact_amount = $receiver_transactions['amount'];
				$sender_transact_total = $sender_transactions['total'];
				$sender_transact_amount = $sender_transactions['amount'];

				if ($this->config->get('wk_wallet_system_transaction_status')) {
					$total_amount_limit = (float)$this->config->get('wk_wallet_system_amount_limit');

					if ($receiver_transact_total >= (float)$this->config->get('wk_wallet_system_transact_limit')) {
						$json['error'] = $receiver_error = html_entity_decode(sprintf($this->language->get('error_transaction_number_r'), $receiver_name, $this->url->link('information/contact', '', true)));
					}
					if ($receiver_transact_amount >= (float)$this->config->get('wk_wallet_system_amount_limit')) {
						$json['error'] = $receiver_error = html_entity_decode(sprintf($this->language->get('error_transaction_amount_r'), $receiver_name, $this->url->link('information/contact', '', true)));
					}

					if ($sender_transact_total >= (float)$this->config->get('wk_wallet_system_transact_limit')) {
						$json['error'] = $sender_error = html_entity_decode(sprintf($this->language->get('error_transaction_number'), $this->url->link('information/contact', '', true)));
					}
					if ($sender_transact_amount >= (float)$this->config->get('wk_wallet_system_amount_limit')) {
						$json['error'] = $sender_error = html_entity_decode(sprintf($this->language->get('error_transaction_amount'), $this->url->link('information/contact', '', true)));
					}

					if (!$json) {
						if (!isset($sender_error)) {
							$remaining_amount = $total_amount_limit - $sender_transact_amount;
							if ($remaining_amount < $pay_amount) {
								$json['error'] = sprintf($this->language->get('error_limit_remaining'), $remaining_amount);
							}
						} elseif (!isset($receiver_error)) {
							$remaining_amount = $total_amount_limit - $receiver_transact_amount;
							if ($remaining_amount < $pay_amount) {
								$json['error'] = sprintf($this->language->get('error_limit_remaining_r'), $remaining_amount, $receiver_name);
							}
						}
					}
				}

				if (!$json) {
					if ($this->request->post['receiver_email'] == $this->customer->getEmail()) {
						$json['error'] = $this->language->get('text_same_account');
					} elseif ($pay_amount == '') {
						$json['error'] = $this->language->get('text_enter_amount');
					} elseif (($pay_amount >= 1) && ($pay_amount <= $balance)) {
						$this->model_account_wk_wallet_system->transferWalletAmount($email_info);
						$this->session->data['success'] = $json['success'] = $this->language->get('text_success_transfer');
						$json['redirect'] = html_entity_decode($this->url->link('account/wk_wallet_system', 'transfer=debit', true));
					} else {
						$json['error'] = $this->language->get('text_insufficient_amount');
					}
				}
			} else {
				$json['error'] = $this->language->get('text_email_not_exist');
			}
		} else {
			$json['error'] = $this->language->get('text_password_incorrect');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function payeeList() {
		if (!$this->config->get('total_wk_wallet_system_status')) {
			$this->response->redirect($this->url->link('account/account', '', true));
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/wk_wallet_system/payeeList', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$data = $this->load->language('account/wk_wallet_payee');
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/wk_wallet_system/addPayee', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$data['warning'] = '';
		}

		$this->load->model('account/wk_wallet_system');
		$data['balance'] = $this->model_account_wk_wallet_system->walletBalance();

		$data['transaction_status'] = $this->config->get('wk_wallet_system_transaction_status');

		if ($this->config->get('wk_wallet_system_transaction_status')) {
			$sent_transactions = $this->model_account_wk_wallet_system->sentMonthlyTransactions($this->customer->getId());

			$data['total_transactions'] = $total_transactions = $sent_transactions['total'];
			$data['total_transact_amount'] = $total_transact_amount = $sent_transactions['amount'];
			$data['total_transaction_limit'] = (float)$this->config->get('wk_wallet_system_transact_limit');
			$data['total_amount_limit'] = (float)$this->config->get('wk_wallet_system_amount_limit');

			if ($total_transactions >= (float)$this->config->get('wk_wallet_system_transact_limit')) {
				$data['transaction_error'] = sprintf($this->language->get('error_transaction_number'), $this->url->link('information/contact', '', true));
			}
			if ($total_transact_amount >= (float)$this->config->get('wk_wallet_system_amount_limit')) {
				$data['transaction_error'] = sprintf($this->language->get('error_transaction_amount'), $this->url->link('information/contact', '', true));
			}

			$data['minimum_transaction_amount'] = $this->config->get('wk_wallet_system_min_transact');
			$data['maximum_transaction_amount'] = $this->config->get('wk_wallet_system_max_transact');

			if (!isset($data['transaction_error'])) {
				$data['remaining_amount'] = $data['total_amount_limit'] - $data['total_transact_amount'];
			} else {
				$data['remaining_amount'] = 0;
			}

			$data['remaining_limit'] = sprintf($this->language->get('error_limit_remaining'), $data['remaining_amount']);
			$data['error_amount_limit'] = sprintf($this->language->get('error_amount_limit'), $data['minimum_transaction_amount'], $data['maximum_transaction_amount']);
		}

		$data['payee'] = $this->model_account_wk_wallet_system->payeeList();

		if (isset($this->request->get['filter_email'])) {
			$data['filter_email'] = $this->request->get['filter_email'];
		} else {
			$data['filter_email'] = null;
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/wk_wallet_payee', $data));
	}

	public function deletePayee() {
		$json = array();
		$this->load->language('account/wk_wallet_payee');

		if (!$this->customer->isLogged()) {
			$json['error'] = $this->language->get('error_login');
			$json['location'] = $this->url->link('account/wk_wallet_system/payeeList', '', true);
		}

		if (!$json && isset($this->request->post['payee_id']) && $this->request->post['payee_id']) {
			$this->load->model('account/wk_wallet_system');
			$result = $this->model_account_wk_wallet_system->deletePayee($this->request->post['payee_id']);

			if ($result) {
				$json['success'] = $this->session->data['success'] = $this->language->get('text_delete_payee_success');
			} else {
				$json['error'] = $this->language->get('text_delete_payee_error');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function addPayee() {
		$json = array();
		$this->load->language('account/wk_wallet_payee');

		if (!$this->customer->isLogged()) {
			$json['error'] = $this->language->get('error_login');
			$json['location'] = $this->url->link('account/wk_wallet_system/payeeList', '', true);
		}

		if (!$json) {
			if (isset($this->request->post['payee_email']) && $this->request->post['payee_email']) {
				$payee_email = $this->request->post['payee_email'];

				$this->load->model('account/wk_wallet_system');
				$result = $this->model_account_wk_wallet_system->addPayee($payee_email);

				if ($result) {
					$json['success'] = $this->session->data['success'] = $this->language->get('text_success_payee');
				} else {
					$json['error'] = $this->language->get('text_payee_already_exist');
				}
			} else {
				$json['error'] = $this->language->get('text_provide_email');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function confirmPayee() {
		$json = array();
		$this->load->language('account/wk_wallet_payee');

		if (!$this->customer->isLogged()) {
			$json['error'] = $this->language->get('error_login');
			$json['location'] = $this->url->link('account/wk_wallet_system/payeeList', '', true);
		}

		if (!$json) {
			if ($this->request->post['payee_email'] == $this->customer->getEmail()) {
				$json['error'] = $this->language->get('text_cannot_add_yourself');
			} elseif ($this->request->post['payee_email'] == '') {
				$json['error'] = $this->language->get('text_provide_email');
			} else {
				$this->load->model('account/wk_wallet_system');
				$checkPayee = $this->model_account_wk_wallet_system->checkPayee($this->request->post['payee_email']);

				if ($checkPayee) {
					$json['error'] = $this->language->get('text_payee_already_exist');
				} else {
					$result = $this->model_account_wk_wallet_system->confirmPayee($this->request->post['payee_email']);
					if(isset($result['name'])) {
						$json['success'] = 1;
						$json['name'] = $result['name'];
						$json['email'] = $this->request->post['payee_email'];
					} else {
						$json['error'] = $this->language->get('text_payee_not_exist');
					}
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function useWallet()	{
		$this->load->language('account/wk_wallet_system');
		$json = array();

		if ($this->customer->getId()) {
			$this->load->model('account/wk_wallet_system');
			$wallet_balance = $this->model_account_wk_wallet_system->walletBalance();

			if (isset($this->request->post['isWallet'])) {
				if ($this->request->post['isWallet'] == 'true') {
					unset($this->session->data['wallet_deduction']);
					unset($this->session->data['wk_cart_amount']);
					$this->session->data['wk_wallet_payment'] = true;
				} else {
					unset($this->session->data['wk_wallet_payment']);
					unset($this->session->data['wallet_deduction']);
					unset($this->session->data['wk_cart_amount']);
					$json['text'] = $this->language->get('text_preffered_method');
				}
			}

			// Totals
			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			$this->load->model('setting/extension');

			$sort_order = array();

			$results = $this->model_setting_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get('total_' . $result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			if (isset($this->session->data['wk_wallet_payment'])) {
				if (isset($this->session->data['wk_cart_amount']) && ($this->session->data['wk_cart_amount'] <= $wallet_balance)) {
					$result['code'] = 'wk_wallet_system_payment';
					$this->load->model('extension/payment/' . $result['code']);
					$method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

					if ($method) {
						$method_data[$result['code']] = $method;
					}
					$json['payment_methods'] = $this->session->data['payment_methods'] = $method_data;
				} else {
					$json['text'] = $this->language->get('text_preffered_method1');
				}
			}
		}

		if (isset($json['text'])) {
			// Payment Methods
			$method_data = array();

			$this->load->model('setting/extension');

			$results = $this->model_setting_extension->getExtensions('payment');
			$recurring = $this->cart->hasRecurringProducts();

			foreach ($results as $result) {
				if ($this->config->get('payment_' . $result['code'] . '_status')) {
					$this->load->model('extension/payment/' . $result['code']);

					$method = $this->{'model_extension_payment_' . $result['code']}->getMethod($this->session->data['payment_address'], $total);

					if ($method) {
						if ($recurring) {
							if (property_exists($this->{'model_extension_payment_' . $result['code']}, 'recurringPayments') && $this->{'model_extension_payment_' . $result['code']}->recurringPayments()) {
								$method_data[$result['code']] = $method;
							}
						} else {
							$method_data[$result['code']] = $method;
						}
					}
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);
			$json['payment_methods'] = $this->session->data['payment_methods'] = $method_data;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function updateBankDetails()	{
		$this->load->language('account/wk_wallet_system');

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->load->model('account/wk_wallet_system');
			$this->model_account_wk_wallet_system->updateBankDetails($this->request->post, $this->customer->getId());
			$this->session->data['success'] = $this->language->get('text_bank_success');
		} else {
			$this->session->data['warning'] = $this->language->get('text_warning_bank');
		}

		$this->response->redirect($this->url->link('account/wk_wallet_system', '', true));
	}

	private function validate() {
		if (!isset($this->request->post['bank_name'])) {
			return false;
		}
		if (!isset($this->request->post['bank_branch_number'])) {
			return false;
		}
		if (!isset($this->request->post['bank_swift_code'])) {
			return false;
		}
		if (!isset($this->request->post['bank_account_name'])) {
			return false;
		}
		if (!isset($this->request->post['bank_account_number'])) {
			return false;
		}

		return true;
	}
}
