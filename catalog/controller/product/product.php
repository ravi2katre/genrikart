<?php
class ControllerProductProduct extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('product/product');
		$data['order_sub_total'] = $this->cart->getSubTotal();
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$this->load->model('catalog/category');

		if (isset($this->request->get['path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = $path_id;
				} else {
					$path .= '_' . $path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path)
					);
				}
			}

			// Set the last category breadcrumb
			$category_info = $this->model_catalog_category->getCategory($category_id);

			if ($category_info) {
				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				if (isset($this->request->get['limit'])) {
					$url .= '&limit=' . $this->request->get['limit'];
				}

				$data['breadcrumbs'][] = array(
					'text' => $category_info['name'],
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
				);
			}
		}

		$this->load->model('catalog/manufacturer');

		if (isset($this->request->get['manufacturer_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_brand'),
				'href' => $this->url->link('product/manufacturer')
			);

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

			if ($manufacturer_info) {
				$data['breadcrumbs'][] = array(
					'text' => $manufacturer_info['name'],
					'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
				);
			}
		}

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_search'),
				'href' => $this->url->link('product/search', $url)
			);
		}
		
		$data['cart_total'] = $this->cart->getTotal();
		if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
			if ($product_id ==58 && !$this->customer->isLogged()) {
				$this->session->data['redirect'] = 'index.php?route=product/product&product_id=58';
				$cookie_name = "redirect_customer";
				$cookie_value = 'product/product&product_id=58';
				setcookie($cookie_name, $cookie_value, time() + (3 * 10), "/"); // 86400 = 1 day

				$this->response->redirect($this->url->link('account/account', '', true));
			}else{
				//$data['customer'][''] = $this->customer;
				$data['customer']['id'] = $this->customer->getId();
				$data['customer']['first_name'] = $this->customer->getFirstName();
				$data['customer']['last_name'] = $this->customer->getLastName();
				$data['customer']['email'] = $this->customer->getEmail();
				$data['customer']['phone'] = $this->customer->getTelephone();
				
				$data['app'] = $_GET;
				//print_r($data['customer']); exit;
			}
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);
		$data['discounts_list'] = $this->model_catalog_product->getProductCoupons($product_id);

		if ($product_info) {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $product_info['name'],
				'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
			);

			$this->document->setTitle($product_info['meta_title']);
			$this->document->setDescription($product_info['meta_description']);
			$this->document->setKeywords($product_info['meta_keyword']);
			$this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
			$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment.min.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment/moment-with-locales.min.js');
			$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
			$this->document->addScript('catalog/view/javascript/jquery.steps-1.1.0/jquery.steps.min.js');
			$this->document->addStyle('catalog/view/javascript/jquery.steps-1.1.0/jquery.steps.css');
			$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

			$data['heading_title'] = $product_info['name'];
			$data['sku'] = $product_info['sku'];

			$data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			$data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));

			$this->load->model('catalog/review');

			$data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

			$data['product_id'] = (int)$this->request->get['product_id'];
			$data['manufacturer'] = $product_info['manufacturer'];
			$data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$data['model'] = $product_info['model'];
			$data['reward'] = $product_info['reward'];
			$data['points'] = $product_info['points'];
			$data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

			if ($product_info['quantity'] <= 0) {
				$data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$data['stock'] = $product_info['quantity'];
			} else {
				$data['stock'] = $this->language->get('text_instock');
			}

			$this->load->model('tool/image');

			if ($product_info['image']) {
				$data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height'));
			} else {
				$data['popup'] = '';
			}

			if ($product_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
			} else {
				$data['thumb'] = '';
			}

			$data['images'] = array();

			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

			foreach ($results as $result) {
				$data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_additional_height'))
				);
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['price'] = false;
			}

			if ((float)$product_info['special']) {
				$data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$data['special'] = false;
			}

			if ($this->config->get('config_tax')) {
				$data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
			} else {
				$data['tax'] = false;
			}

			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

			$data['discounts'] = array();

			foreach ($discounts as $discount) {
				$data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
				);
			}

			$data['options'] = array();

			foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

			if ($product_info['minimum']) {
				$data['minimum'] = $product_info['minimum'];
			} else {
				$data['minimum'] = 1;
			}

			$data['review_status'] = $this->config->get('config_review_status');

			if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
				$data['review_guest'] = true;
			} else {
				$data['review_guest'] = false;
			}

			if ($this->customer->isLogged()) {
				$data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
			} else {
				$data['customer_name'] = '';
			}

			$data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$data['rating'] = (int)$product_info['rating'];

			// Captcha
			if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
			} else {
				$data['captcha'] = '';
			}

			$data['share'] = $this->url->link('product/product', 'product_id=' . (int)$this->request->get['product_id']);

			$data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

			$data['products'] = array();



			/*$results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}*/


			$getProductRelatedCategoryresults = $this->model_catalog_product->getProductsProductByMolecule($this->request->get['product_id']);
			
			foreach ($getProductRelatedCategoryresults as $result) {
				
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_related_height'));
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = '';
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$data['products_related_category'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,					
					'rating'      => $rating,
					'manufacturer'=> $result['manufacturer'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			$data['tags'] = array();

			if ($product_info['tag']) {
				$tags = explode(',', $product_info['tag']);

				foreach ($tags as $tag) {
					$data['tags'][] = array(
						'tag'  => trim($tag),
						'href' => $this->url->link('product/search', 'tag=' . trim($tag))
					);
				}
			}

			$data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

			$this->model_catalog_product->updateViewed($this->request->get['product_id']);
			$data['PRODUCT_DETAIL_OPTION'] = array(
				"SELECTED_OPTION_ID"=>231,
				"FILE_OPTION_ID"=>228,
				//"STEP_2_SPECIFY_OPTION_ID"=>237,
				"NUMBER_OF_MEDITIONS_OPTION_ID"=>232,
				"PRISCRIPTION_DETAILS_OPTION_ID"=>233,

			);
			$data['upload_prescription']['step2_options'] = array(
				"1" => array('text'=>'Order all medicine as per prescription', 'option_id'=>"232",  'sub_values'=>array()),
				"2" => array('text'=>'Order all medicines(Substitutes) as per prescription', 'option_id'=>"232", 'sub_values'=>array()),
				"3" => array('text'=>"Specify medicine", 'option_id'=>"233", 'sub_values' =>array(
					'Specify medicine - Need any other medicine in prescription', 
					'Specify medicine - Need any other medicine(Substitute) in prescription') ),
				"4" => array('text'=>"Specify medicine", 'option_id'=>"233", 'sub_values' =>array(
					'Require any other medicine in prescription', 
					'Specify medicine - Substitute medicine as per proscription') ),
				"5" => array('text'=>"Call me for details", 'option_id'=>"232", 'sub_values'=>array())
			);
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('product/product', $data));
			
			
		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['manufacturer_id'])) {
				$url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
			}

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . $this->request->get['search'];
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . $this->request->get['tag'];
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function review() {
		$this->load->language('product/product');

		$this->load->model('catalog/review');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['reviews'] = array();

		$review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

		$results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
			$data['reviews'][] = array(
				'author'     => $result['author'],
				'text'       => nl2br($result['text']),
				'rating'     => (int)$result['rating'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$pagination = new Pagination();
		$pagination->total = $review_total;
		$pagination->page = $page;
		$pagination->limit = 5;
		$pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

		$this->response->setOutput($this->load->view('product/review', $data));
	}

	public function write() {
		$this->load->language('product/product');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_text');
			}

			if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
				$json['error'] = $this->language->get('error_rating');
			}

			// Captcha
			if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
				$captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

				if ($captcha) {
					$json['error'] = $captcha;
				}
			}

			if (!isset($json['error'])) {
				$this->load->model('catalog/review');

				$this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

				$json['success'] = $this->language->get('text_success');
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getRecurringDescription() {
		$this->load->language('product/product');
		$this->load->model('catalog/product');

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		if (isset($this->request->post['recurring_id'])) {
			$recurring_id = $this->request->post['recurring_id'];
		} else {
			$recurring_id = 0;
		}

		if (isset($this->request->post['quantity'])) {
			$quantity = $this->request->post['quantity'];
		} else {
			$quantity = 1;
		}

		$product_info = $this->model_catalog_product->getProduct($product_id);

		$recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

		$json = array();

		if ($product_info && $recurring_info) {
			if (!$json) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($recurring_info['trial_status'] == 1) {
					$price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
				} else {
					$trial_text = '';
				}

				$price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

				if ($recurring_info['duration']) {
					$text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				} else {
					$text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
				}

				$json['success'] = $text;
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	function get_saved_files(){
		$this->load->language('product/product');
		$this->load->model('catalog/product');
		$customer_id = $this->session->data['customer_id'];
		$list = $this->model_catalog_product->getCustomerFiles($customer_id);
		//$list = $this->model_catalog_product->get_prescription($customer_id);
		

		//print_r($list); exit;
		$json = array();
		$json['list'] = $list;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	
	function get_category_products(){

		$data['theme_path'] = $this->config->get('theme_path');
        $data['site_url'] = $this->config->get('site_url');
		$this->load->language('extension/module/featured');


        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $this->load->model('tool/image');

        $data['products'] = array();



		$cat_id = $this->request->post['cat_id'];
		$start = $this->request->post['start'];
		$limit = $this->request->post['limit'];

		$display_categories = $this->model_catalog_category->getDisplayCategories($cat_id);
        foreach ($display_categories as $key => $val) {
			$data['filter_category_id'] = $val['category_id'];
			$data['start'] = $start;
			$data['limit'] = $limit;

            $display_categories[$key]['products'] = $this->model_catalog_product->getProducts($data);
            if (count($display_categories[$key]['products']) > 0) {
                foreach ($display_categories[$key]['products'] as $key2 => $val2) {

                    $product_info = $this->model_catalog_product->getProduct($val2['product_id']);

                    if ($product_info) {
                        if ($product_info['image']) {
                            $image =  $this->model_tool_image->resize($product_info['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_thumb_height'));
                        } else {
                            $image = '';
						}




                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }

                        if ((float) $product_info['special']) {
                            $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $special = false;
                        }

                        if ($this->config->get('config_tax')) {
                            $tax = $this->currency->format((float) $product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                        } else {
                            $tax = false;
                        }

                        if ($this->config->get('config_review_status')) {
                            $rating = $product_info['rating'];
                        } else {
                            $rating = false;
                        }

                        $data2 = array(
                            'product_id' => $product_info['product_id'],
                            'thumb' => $image,
                            'name' => $product_info['name'],
                            'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                            'price' => $price,
							'special' => $special,
							'org_price' => $product_info['price'],
                            'org_special' => $product_info['special'],
                            'save_price' => $this->currency->format($product_info['price'] - $product_info['special'], $this->session->data['currency']),
                            'tax' => $tax,
                            'rating' => $rating,
                            'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        );

                        $display_categories[$key]['products'][$key2] = $data2;
                    }

                }
            }
        }
        $data['display_categories'] = $display_categories;




		//print_r($list); exit;
		$json = array();
		$json['list'] =  $display_categories[$key]['products'];

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	function set_product_pd_update(){
		//SELECT * FROM `oc_product_attribute`
  echo "SELECT product_id,text FROM " . DB_PREFIX . "product_attribute pa WHERE pa.attribute_id = 12";
		$query = $this->db->query("SELECT product_id, text FROM " . DB_PREFIX . "product_attribute pa WHERE pa.attribute_id = 12 limit 50000,50000");
		$rows = $query->rows;
		//var_dump($rows); exit;
  
		foreach($rows as $key=>$val){
			//print_r($val); exit;
   $sql = "UPDATE oc_product_description SET composition = '" .$val['text']. "' WHERE product_id = '" . (int)$val['product_id']. "' AND language_id = 1";
   $this->db->query($sql);
   echo (int)$val['product_id']." <br>";
		}
		echo "Done"; exit;
	}
	
	function option_clenup(){
		$query = $this->db->query("SELECT product_option_id FROM `oc_product_option` where option_id = 13 group by `product_id` having count(product_id) > 1");
		$rows = $query->rows;
		//var_dump($rows); exit;
		foreach($rows as $key=>$val){
			$this->db->query("DELETE FROM `oc_product_option` WHERE `oc_product_option`.`product_option_id` =". $val['product_option_id']);
		}
		echo "Done"; exit;
	}
	
	function add_prescription(){
			$data = array(
				'customer_id' =>  $this->request->post['customer_id'],
				'code' => $this->request->post['code'],
			);

			/*$data = array(
				'customer_id' =>  1,
				'code' => '99769d4fa491a53156eb076e3519bcfe87680d29',
			);*/
			//print_r($data); 
			$this->load->model('catalog/product');			 
			$this->model_catalog_product->prescriptions($data);
			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
	}
	
	function add_skip_products(){
		$group_name = 'spacial';
		echo "SELECT p.product_id, ops.group_name,
		(SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
		
		FROM oc_product p 	
		LEFT JOIN oc_products_skiped ops ON ops.product_id = p.product_id 		
		where p.product_id != 58
		group by p.product_id
		having special IS NULL
		";
		 exit;
		$query = $this->db->query("SELECT p.product_id, ops.group_name,
		(SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special
		
		FROM oc_product p 	
		LEFT JOIN oc_products_skiped ops ON ops.product_id = p.product_id 		
		where p.product_id = 1
		group by p.product_id
		having special <= 0
		");
		
		
		$rows = $query->rows;
		//var_dump($rows); exit;
		foreach($rows as $key=>$val){
			if(empty($val['group_name'])){
				$this->db->query("INSERT INTO " . DB_PREFIX . "products_skiped SET product_id = '" . $val['product_id'] . "', group_name = '" . $group_name . "'");
			}
		}
		
		echo "Done"; exit;
	}
	
	function coupen_apply(){
		$this->load->model('catalog/product');
		$query = $this->db->query("SELECT name  from test2 ");
		$rows = $query->rows;
		//var_dump($rows); exit;
		foreach($rows as $key=>$val){
			if(!empty($val['name'])){
				
				

				$parameters = array();
				$parameters["filter_name"] = $val['name'];
				$parameters["start"] = 1;
				$parameters["limit"] = 1000;
				$rows2 = $this->model_catalog_product->getProductsSearch($parameters);
				unset($rows2['product_total']);
				//print_r($rows2); exit;
				foreach($rows2 as $key2=>$val2){
						
						$data = array(
						'coupon_id' => 6,
						'product_id' => $val2['product_id']
						);
						$this->insert_product_coupen($data);
				}
				
			}
		}
		echo "Done"; exit;
			
	}
 
 
 function desable_nrx(){
		$this->load->model('catalog/product');
		$query = $this->db->query("SELECT name  from test3_nrx_list ");
		$rows = $query->rows;
		//var_dump($rows); exit;
		foreach($rows as $key=>$val){
			if(!empty($val['name'])){
				
				

				$parameters = array();
				$parameters["filter_name"] = $val['name'];
				$parameters["start"] = 1;
				$parameters["limit"] = 1000;
				$rows2 = $this->model_catalog_product->getProductsSearch($parameters);
				unset($rows2['product_total']);
				//print_r($rows2); 
				foreach($rows2 as $key2=>$val2){
						
						$data = array(
						'coupon_id' => 6,
						'product_id' => $val2['product_id']
						);
						$this->db->query("UPDATE `oc_product` SET `status` = '0' WHERE `oc_product`.`product_id` = ".$val2['product_id']);
				}
				
			}
		}
		echo "Done"; exit;
			
	}
	
	function insert_product_coupen($data=array()){
		print_r($data);
		$query = $this->db->query("SELECT * FROM `oc_coupon_product` where coupon_id = ".$data['coupon_id']." and product_id = ".$data['product_id']);
		$row = $query->row;
		//print_r($row ); 
		if(count($row) == 0){
			$this->db->query("INSERT INTO " . DB_PREFIX . "coupon_product SET coupon_id = '" . $data['coupon_id'] . "', product_id = '" . $data['product_id'] . "'");
		}
		echo "insert_product_coupen".$data['product_id'].PHP_EOL; 
	}
	
	function sms(){
		$data = array(
			'phone' => "9881815256",
			'msg' => "Test Message from API",
		);

		smsRequest($data);

	}
 
 function add_seo_url_categories(){
		
		/*$query = $this->db->query("SELECT p.product_id, ops.group_name,
		
		
		FROM oc_product p 	
		LEFT JOIN oc_products_skiped ops ON ops.product_id = p.product_id 		
		where p.product_id = 1
		group by p.product_id
		having special <= 0
		");*/
  
  $query = $this->db->query("SELECT c.name, c.category_id
		FROM oc_category_description c	");
		
		
		$rows = $query->rows;
		//var_dump($rows); exit;
		foreach($rows as $key => $val){
			if(!empty($val['name'])){
      
      $set_insert_field = '';
      
      $slug  = trim($this->format_uri($val['name']), '-');
      $set_insert_field .= "keyword = '" . $slug . "'";
      
      $query_filed = 'category_id='.$val['category_id'];
      $set_insert_field .= ", query = '" . $query_filed . "'";
      
      $set_insert_field .= ", store_id = 0";
      $set_insert_field .= ", language_id = 1";
      //echo $set_insert_field .PHP_EOL;
      $query = $this->db->query("SELECT keyword	FROM oc_seo_url where keyword ='". $slug."'	");
		
      $row = $query->row;
      print_r($row);
      if(!isset($row['keyword'])){
         $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET ".$set_insert_field);
      }
      
			}
		}
		
		echo "Done"; exit;
	}
 
 function add_seo_url_products(){
		
		/*$query = $this->db->query("SELECT p.product_id, ops.group_name,
		
		
		FROM oc_product p 	
		LEFT JOIN oc_products_skiped ops ON ops.product_id = p.product_id 		
		where p.product_id = 1
		group by p.product_id
		having special <= 0
		");*/
  
  $query = $this->db->query("SELECT p.name, p.product_id
		FROM oc_product_description p	limit 130000, 10000");
		
		
		$rows = $query->rows;
		//var_dump($rows); exit;
		foreach($rows as $key => $val){
			if(!empty($val['name'])){
      
      $set_insert_field = '';
      
      $slug  = trim($this->format_uri($val['name']), '-');
      $set_insert_field .= "keyword = '" . $slug . "'";
      
      $query_filed = 'product_id='.$val['product_id'];
      $set_insert_field .= ", query = '" . $query_filed . "'";
      
      $set_insert_field .= ", store_id = 0";
      $set_insert_field .= ", language_id = 1";
      //echo $set_insert_field .PHP_EOL;
      $query = $this->db->query("SELECT keyword	FROM oc_seo_url where keyword ='". $slug."'	");
		
      $row = $query->row;
      //print_r($row);
      if(!isset($row['keyword'])){
         $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET ".$set_insert_field);
      }
      
			}
		}
		
		echo "Done"; exit;
	}
 
 function format_uri( $string, $separator = '-' )
{
    $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
    $special_cases = array( '&' => 'and', "'" => '');
    $string = mb_strtolower( trim( $string ), 'UTF-8' );
    $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
    $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
    $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
    $string = preg_replace("/[$separator]+/u", "$separator", $string);
    return $string;
}
 
}
