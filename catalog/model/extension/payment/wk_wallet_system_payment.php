<?php
class ModelExtensionPaymentWkwalletsystempayment extends Model {
	public function getMethod($address, $total) {

		$method_data = array(
			'code'       => 'wk_wallet_system_payment',
			'title'      => $this->config->get('wk_wallet_system_payment_name') ? $this->config->get('wk_wallet_system_payment_name') : 'Wallet',
			'terms'      => '',
			'sort_order' => $this->config->get('wk_wallet_system_sort_order')
		);

		return $method_data;
	}
}
