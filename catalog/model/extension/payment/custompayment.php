<?php
class ModelExtensionPaymentCustomPayment extends Model {
  public function getMethod($address, $total) {
    $this->load->language('extension/payment/custompayment');

    // $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_custompayment_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

    if ($this->config->get('payment_custompayment_total') > 0 && $this->config->get('payment_custompayment_total') > $total) {
      $status = false;
    } elseif (!$this->config->get('payment_custompayment_geo_zone_id')) {
      $status = true;
    } elseif ($query->num_rows) {
      $status = true;
    } else {
      $status = false;
    }

    $method_data = array();

    if ($status) {
      $method_data = array(
        'code'       => 'custompayment',
        'title'      => $this->language->get('text_title').$this->language->get('img_title'),
        'terms'      => '',
        'class'      => 'payment',
        'sort_order' => $this->config->get('payment_custompayment_sort_order')
      );
    }


    $pm_type = $this->config->get('payment_custompayment_merchant_type');

 
    if ($pm_type['twoparty'] == 1) {
      $payment_methods_array['types'][] = 'two_party';
    }

    if ($pm_type['threeparty'] == 1) {
      $payment_methods_array['types'][] = 'three_party';
    }

 
    return $method_data;
  }
}

?>
