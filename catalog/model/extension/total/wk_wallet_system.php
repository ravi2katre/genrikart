<?php
class ModelExtensionTotalWkwalletsystem extends Model {
	public function getTotal($total) {
		if ($this->customer->getId() && isset($this->session->data['wk_wallet_payment'])) {
			$isWallet = false;
			$has_product = $this->cart->hasProducts();

			if ($has_product && ($has_product == 1)) {
				$products = $this->cart->getProducts();
				if ($products[0]['product_id'] == $this->config->get('wallet_product_id')) {
					$isWallet = true;
				}
			}

			if (!$isWallet) {
				$this->load->model('setting/extension');
				$results = $this->model_setting_extension->getExtensions('total');
				$totals = array();

				$taxes = $this->cart->getTaxes();
				$totall = 0;
				$wallet = 0;

				// Because __call can not keep var references so we put them into an array.
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$totall
				);

				foreach ($results as $result) {
					if ($result['code'] == 'wk_wallet_system' || $result['code'] == 'total') {
						continue;
					}
					if ($this->config->get('total_' . $result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				foreach ($totals as $value) {
					$wallet += $value['value'];
				}

				$wallet = $this->currency->convert($wallet, $this->config->get('config_currency'), $this->session->data['currency']);

				$this->load->model('account/wk_wallet_system');
				$wallet_balance = $this->model_account_wk_wallet_system->walletBalance();

				if ($wallet <= $wallet_balance) {
					$this->session->data['wk_cart_amount'] = $wallet;
				} else {
					$this->session->data['wk_cart_amount'] = $wallet;
					$wallet = $wallet_balance;
				}

				$this->session->data['wallet_deduction'] = $wallet;

				$wallet = $this->currency->convert($wallet, $this->session->data['currency'], $this->config->get('config_currency'));

				$total['totals'][] = array(
					'code'       => 'wk_wallet_system',
					'title'      => $this->config->get('wk_wallet_system_total_name'),
					'value'      => -$wallet,
					'sort_order' => $this->config->get('wk_wallet_system_sort_order')
				);

				$total['total'] -= $wallet;
			}
		}
	}
}
