<?php
class ModelToolUpload extends Model {
	public function addUpload($name, $filename) {
		$code = sha1(uniqid(mt_rand(), true));

		$this->db->query("INSERT INTO `" . DB_PREFIX . "upload` SET `name` = '" . $this->db->escape($name) . "', `filename` = '" . $this->db->escape($filename) . "', `code` = '" . $this->db->escape($code) . "', `date_added` = NOW()");

		return $code;
	}

	public function getUploadByCode($code) {				$codes =array_filter( explode(',', $code));		if(count($codes) > 1){			$codes = implode("','", array_map('trim',$codes));						$query = $this->db->query("SELECT *, GROUP_CONCAT(name) as name, GROUP_CONCAT(code) as code FROM `" . DB_PREFIX . "upload` WHERE code in( '" . $codes . "' ) group by date(date_added)");					}else{			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "upload` WHERE code = '" . $this->db->escape($code) . "'");		}
		return $query->row;
	}
}