<?php
class ModelAccountWkwalletsystem extends Model {
/**
 * sends an email on successful transfer to wallet
 * @param array $data contains details sent in custom variable
 */
	public function sendMail($data)	{
		if ($data) {
			$customer_id = $data['customer_id'];
			$amount = $data['amount'];
			$currency_code = $data['currency_code'];

			$this->load->language('account/wk_wallet_system');
			$this->load->model('account/customer');
			$customer_info = $this->model_account_customer->getCustomer($customer_id);

			$message  = sprintf($this->language->get('text_dear'), $customer_info['firstname'] . ' ' . $customer_info['lastname']) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $currency_code, 1)) . "\n\n";
			$message .= sprintf($this->language->get('text_current_balance'), $this->currency->format(($this->walletBalance($customer_id) + $amount), $currency_code, 1)) . "\n\n";
			$message .= $this->url->link('account/wk_wallet_system', '', true) . "\n\n";
			$message .= $this->language->get('text_regards') . "\n\n";
			$message .= html_entity_decode($this->config->get('config_name'));

			/**
			 * mail to customer
			 */

	 		$mail = new Mail($this->config->get('config_mail_engine'));
	 		$mail->parameter = $this->config->get('config_mail_parameter');
	 		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
	 		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
	 		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
	 		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
	 		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
			$mail->setText($message);
			$mail->send();
		}
	}
/**
 * sends an email on successful deduction to wallet
 * @param array $data contains details sent in custom variable
 */
	public function sendDeductMail($zero_balance = 0) {
		$customer_id = $this->customer->getId();
		$amount = $this->session->data['wallet_deduction'];
		$currency_code = $this->session->data['currency'];

		$this->load->language('account/wk_wallet_system');
		$this->load->model('account/customer');
		$customer_info = $this->model_account_customer->getCustomer($customer_id);

		$message  = sprintf($this->language->get('text_dear'), $customer_info['firstname'] . ' ' . $customer_info['lastname']) . "\n\n";
		$message .= sprintf($this->language->get('text_transaction_deduct'), $this->currency->format($amount, $currency_code, 1)) . "\n\n";
		if ($zero_balance) {
			$message .= sprintf($this->language->get('text_current_balance'), $this->currency->format(0, $currency_code, 1)) . "\n\n";
		} else {
			$message .= sprintf($this->language->get('text_current_balance'), $this->currency->format(($this->walletBalance($customer_id) - $amount), $currency_code, 1)) . "\n\n";
		}
		$message .= $this->url->link('account/wk_wallet_system', '', true) . "\n\n";
		$message .= $this->language->get('text_regards') . "\n\n";
		$message .= html_entity_decode($this->config->get('config_name'));

		/**
		 * mail to customer
		 */

 		$mail = new Mail($this->config->get('config_mail_engine'));
 		$mail->parameter = $this->config->get('config_mail_parameter');
 		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
 		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
 		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
 		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
 		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($this->customer->getEmail());
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
		$mail->setText($message);
		$mail->send();
	}
/**
 * Fetches customer's wallet balance
 * @return float contains wallet balance
 */
	public function walletBalance()	{
		$credit_amount = 0;
		$debit_amount = 0;

		$total_credits = $this->db->query("SELECT ww.amount, o.currency_code, o.currency_value FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) WHERE ww.type = 'C' AND ww.customer_id = '" . $this->customer->getId() . "' AND status = '1' AND zero_wallet = '0'")->rows;

		foreach ($total_credits as $credit) {
			if (!$credit['currency_code']) {
				$credit['currency_code'] = $this->config->get('config_currency');
			}
			$credit_amount += $this->currency->convert($credit['amount'], $credit['currency_code'], $this->session->data['currency']);
		}

		$total_debits = $this->db->query("SELECT ww.amount, o.currency_code, o.currency_value FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) WHERE ww.type = 'D' AND ww.customer_id = '" . $this->customer->getId() . "' AND status = '1' AND zero_wallet = '0'")->rows;

		foreach ($total_debits as $debit) {
			if (!$debit['currency_code']) {
				$debit['currency_code'] = $this->config->get('config_currency');
			}
			$debit_amount += $this->currency->convert($debit['amount'], $debit['currency_code'], $this->session->data['currency']);
		}

		$balance = $credit_amount - $debit_amount;

		return round($balance, 2);
	}
/**
 * Adds debit history to table on an order
 * @param int  $order_id contains order's ID
 */
	public function addOrderHistory($order_id)	{
		$this->db->query("INSERT INTO " . DB_PREFIX . "wk_wallet SET customer_id = '" . $this->customer->getId() . "', type = 'D', amount = '" . $this->session->data['wallet_deduction'] . "', order_id = '" . (int)$order_id . "', date = '" . date('Y-m-d H:i:s') . "', status = '1', description = 'Paid against order #" . $order_id . "' ");

		if ($this->walletBalance() == '0.00') {
			$this->db->query("UPDATE " . DB_PREFIX . "wk_wallet SET zero_wallet = '1' WHERE customer_id = '" . $this->customer->getId() . "' AND zero_wallet = '0'");
		}

		unset($this->session->data['wallet_deduction']);
		unset($this->session->data['wk_cart_amount']);
		unset($this->session->data['wk_wallet_payment']);
	}

	public function registerWalletAmount($customer_id, $amount) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "wk_wallet SET customer_id = '" . $customer_id . "', type = 'C', amount = '" . $amount . "', date = '" . date('Y-m-d H:i:s') . "', status = '1', description = 'Paid for new registration' ");

		unset($this->session->data['wallet_deduction']);
		unset($this->session->data['wk_cart_amount']);
		unset($this->session->data['wk_wallet_payment']);
	}

	public function checkPassword($password) {
		$customer_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($this->customer->getEmail())) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'")->rows;

		return $customer_query;
	}

	public function transferWalletAmount($data) {
		$this->load->language('account/wk_wallet_system');

		$customer_id = $data['customer_id'];
		$firstname = $data['firstname'];
		$amount = $this->request->post['payee_amount'];
		$email = $this->request->post['receiver_email'];
		$formatted_amount = $this->currency->format($amount, $this->session->data['currency']);

		$transfer_by_desc = sprintf($this->language->get('text_transfer_by'), $this->customer->getEmail());
		$transfer_to_desc = sprintf($this->language->get('text_transfer_to'), $email);

		$this->db->query("INSERT INTO " . DB_PREFIX . "wk_wallet SET customer_id = '" . $customer_id . "', type = 'C', amount = '" . (float)$amount . "', date = '" . date('Y-m-d H:i:s') . "', status = '1', description = '" . $transfer_by_desc . "'");

		if ($this->walletBalance() == '0.00') {
			$this->db->query("UPDATE " . DB_PREFIX . "wk_wallet SET zero_wallet = '1' WHERE customer_id = '" . $customer_id . "' AND zero_wallet = '0'");
		}

		$this->db->query("INSERT INTO " . DB_PREFIX . "wk_wallet_transaction SET sender_id = '" . $this->customer->getId() . "', receiver_id = '" . $customer_id . "', sender_email = '" . $this->customer->getEmail() . "', receiver_email = '" . $email . "', amount = '" . $amount . "', date = '" . date('Y-m-d H:i:s') . "', status = '1'");

		$sender_message  = sprintf($this->language->get('text_dear'), $this->customer->getFirstName()) . "\n\n";
		$sender_message .= sprintf($this->language->get('text_email_transfer'), $formatted_amount, $email) . "\n\n";
		$sender_message .= $this->url->link('account/wk_wallet_system', '', true) . "\n\n";
		$sender_message .= $this->language->get('text_regards') . "\n\n";
		$sender_message .= html_entity_decode($this->config->get('config_name'));

		$this->db->query("INSERT INTO " . DB_PREFIX . "wk_wallet SET customer_id = '" . $this->customer->getId() . "', type = 'D', amount = '" . $amount . "', date = '" . date('Y-m-d H:i:s') . "', status = '1', description = '" . $transfer_to_desc . "' ");

		if ($this->walletBalance() == '0.00') {
			$this->db->query("UPDATE " . DB_PREFIX . "wk_wallet SET zero_wallet = '1' WHERE customer_id = '" . $this->customer->getId() . "' AND zero_wallet = '0'");
		}

		$receiver_message  = sprintf($this->language->get('text_dear'), $firstname ) . "\n\n";
		$receiver_message .= sprintf($this->language->get('text_email_credit'), $formatted_amount, $this->customer->getEmail()) . "\n\n";
		$receiver_message .= $this->url->link('account/wk_wallet_system', '', true) . "\n\n";
		$receiver_message .= $this->language->get('text_regards') . "\n\n";
		$receiver_message .= html_entity_decode($this->config->get('config_name'));

		$mail = new Mail($this->config->get('config_mail_engine'));
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($this->customer->getEmail());
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(sprintf($this->language->get('text_subject_debit'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
		$mail->setText($sender_message);
		$mail->send();

		$mail->setTo($email);
		$mail->setSubject(sprintf($this->language->get('text_subject_credit'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
		$mail->setText($receiver_message);
		$mail->send();
	}

	public function addPayee($payee_email) {
		$payee_exists = $this->db->query("SELECT payee_id FROM " . DB_PREFIX . "wk_wallet_payee WHERE customer_id = '" . $this->customer->getId() . "' AND payee_email = '" . $payee_email . "'")->row;

		if($payee_exists) {
			return false;
		} else {
			$this->db->query("INSERT INTO " . DB_PREFIX . "wk_wallet_payee SET customer_id = '" . $this->customer->getId() . "', payee_email = '" . $payee_email . "', date_added = '" . date('Y-m-d H:i:s') . "'");
			return true;
		}
	}

	public function deletePayee($payee_id) {
		$deleted = $this->db->query("DELETE FROM " . DB_PREFIX . "wk_wallet_payee WHERE payee_id = '" . (int)$payee_id . "' AND customer_id = '" . $this->customer->getId() . "'");

		return $deleted;
	}

	public function checkPayee($payee_email) {
		$payee_exists = $this->db->query("SELECT payee_id FROM " . DB_PREFIX . "wk_wallet_payee WHERE customer_id = '" . $this->customer->getId() . "' AND payee_email = '" . $payee_email . "'")->row;

		if ($payee_exists) {
			return true;
		} else {
			return false;
		}
	}

	public function confirmPayee($payee_email) {
		$customer_detail = $this->db->query("SELECT CONCAT(firstname, ' ', lastname) as name FROM " . DB_PREFIX . "customer WHERE email = '" . $payee_email . "'")->row;

		return $customer_detail;
	}

	public function payeeList() {
		$payees = $this->db->query("SELECT wp.*, CONCAT(c.firstname, ' ', c.lastname) as payee_name FROM " . DB_PREFIX . "wk_wallet_payee wp LEFT JOIN " . DB_PREFIX . "customer c ON (wp.payee_email = c.email) WHERE wp.customer_id = '" . $this->customer->getId() . "'")->rows;

		return $payees;
	}

	public function sentMonthlyTransactions($customer_id) {
		$transactions = $this->db->query("SELECT SUM(amount) as amount, count(*) as total FROM " . DB_PREFIX . "wk_wallet_transaction WHERE sender_id = '" . $customer_id . "' AND date >= '" . date("Y-m-01") . "'" )->row;

		return $transactions;
	}

	public function receivedMonthlyTransactions($customer_id) {
		$transactions = $this->db->query("SELECT SUM(amount) as amount, count(*) as total FROM " . DB_PREFIX . "wk_wallet_transaction WHERE receiver_id = '" . $customer_id . "' AND date >= '" . date("Y-m-01") . "'" )->row;

		return $transactions;
	}
/**
 * Fetches the credit history of a customer
 * @return array contains credit history
 */
	public function getCreditHistory($data = array()) {
		$sql = "SELECT ww.*, o.currency_code FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) WHERE ww.type = 'C' AND ww.customer_id = '" . $this->customer->getId() . "'";

		$sql .= " ORDER BY ww.date DESC";

		if (isset($data['start'])) {
			$sql .= " LIMIT " . $data['start'] . ", 10";
		} else {
			$sql .= " LIMIT 0, 10";
		}

		$creditHistory = $this->db->query($sql)->rows;

		return $creditHistory;
	}
/**
 * get total rows in customer credit
 * @return array returns the credit data of a customer
 */
	public function getTotalCreditHistory()	{
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) WHERE ww.type = 'C' AND ww.customer_id = '" . $this->customer->getId() . "'";

		$creditHistory = $this->db->query($sql)->row;

		return $creditHistory['total'];
	}

/**
 * Fetches the debit history of a customer
 * @return array contains debit history
 */
	public function getDebitHistory($data = array())	{
		$sql = "SELECT ww.*, o.currency_code FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) WHERE ww.type = 'D' AND ww.customer_id = '" . $this->customer->getId() . "' AND status = '1'";

		$sql .= " ORDER BY ww.date DESC";

		if (isset($data['start'])) {
			$sql .= " LIMIT " . $data['start'] . ", 10";
		} else {
			$sql .= " LIMIT 0, 10";
		}

		$debitHistory = $this->db->query($sql)->rows;

		return $debitHistory;
	}

/**
 * gets the total rows of debit of current customer
 * @return array return the debits made by the customer
 */
	public function getTotalDebitHistory()	{
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) WHERE ww.type = 'D' AND ww.customer_id = '" . $this->customer->getId() . "' AND status = '1'";

		$debitHistory = $this->db->query($sql)->row;

		return $debitHistory['total'];
	}

	public function getBankDetails($customer_id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "wallet_bank WHERE customer_id = '" . $customer_id . "'";

		$query = $this->db->query($sql);

		return $query->row;
	}

	public function updateBankDetails($data, $customer_id) {
		$detail_exist = $this->getBankDetails($customer_id);

		if (isset($detail_exist['wallet_bank_id'])) {
			$sql = "UPDATE " . DB_PREFIX . "wallet_bank SET bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "' WHERE wallet_bank_id = '" . $detail_exist['wallet_bank_id'] . "'";
		} else {
			$sql = "INSERT INTO " . DB_PREFIX . "wallet_bank SET bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_branch_number = '" . $this->db->escape($data['bank_branch_number']) . "', bank_swift_code = '" . $this->db->escape($data['bank_swift_code']) . "', bank_account_name = '" . $this->db->escape($data['bank_account_name']) . "', bank_account_number = '" . $this->db->escape($data['bank_account_number']) . "', customer_id = '" . $customer_id . "' ";
		}

		$query = $this->db->query($sql);
	}
}
