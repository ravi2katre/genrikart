<?php
class ControllerExtensionPaymentCustomPayment extends Controller {
  private $error = array();

  public function index() {
    $this->load->language('extension/payment/custompayment');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->load->model('setting/setting');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
      $this->model_setting_setting->editSetting('payment_custompayment', $this->request->post);

      $this->session->data['success'] = $this->language->get('text_success');

      $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
    }

    $data['heading_title'] = $this->language->get('heading_title');
    $data['text_edit'] = $this->language->get('text_edit');

    $data['text_live_mode'] = $this->language->get('text_live_mode');
    $data['text_test_mode'] = $this->language->get('text_test_mode');
    $data['text_enabled'] = $this->language->get('text_enabled');
    $data['text_disabled'] = $this->language->get('text_disabled');
    $data['text_all_zones'] = $this->language->get('text_all_zones');

    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_order_status'] = $this->language->get('entry_order_status');
    $data['entry_order_status_completed_text'] = $this->language->get('entry_order_status_completed_text');
    $data['entry_order_status_pending'] = $this->language->get('entry_order_status_pending');
    $data['entry_order_status_canceled'] = $this->language->get('entry_order_status_canceled');
    $data['entry_order_status_failed'] = $this->language->get('entry_order_status_failed');
    $data['entry_order_status_failed_text'] = $this->language->get('entry_order_status_failed_text');
    $data['entry_order_status_processing'] = $this->language->get('entry_order_status_processing');
    $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
    $data['entry_status'] = $this->language->get('entry_status');
    $data['entry_sort_order'] = $this->language->get('entry_sort_order');
    
     //define marchant type
    $data['entry_merchant_type'] = $this->language->get('entry_merchant_type');
    $data['entry_merchant_type_twoparty'] = $this->language->get('entry_merchant_type_twoparty');
    $data['entry_merchant_type_threeparty'] = $this->language->get('entry_merchant_type_threeparty');

    // $data['entry_payment_type_erip'] = $this->language->get('entry_payment_type_erip');
    $data['button_save'] = $this->language->get('button_save');
    $data['button_cancel'] = $this->language->get('button_cancel');
    $data['tab_general'] = $this->language->get('tab_general');

    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }

    // our merchant data
      if (isset($this->error['Version'])) {
        $data['error_Version'] = $this->error['Version'];
      } else {
        $data['error_Version'] = '';
      }

      if (isset($this->error['AccessCode'])) {
        $data['error_AccessCode'] = $this->error['AccessCode'];
      } else {
        $data['error_AccessCode'] = '';
      }
        
        if (isset($this->error['BankId'])) {
        $data['error_BankId'] = $this->error['BankId'];
      } else {
        $data['error_BankId'] = '';
      }

      if (isset($this->error['TerminalId'])) {
        $data['error_TerminalId'] = $this->error['TerminalId'];
      } else {
        $data['error_TerminalId'] = '';
      }

      if (isset($this->error['MerchantID'])) {
        $data['error_MerchantID'] = $this->error['MerchantID'];
      } else {
        $data['error_MerchantID'] = '';
      }

      if (isset($this->error['MCC'])) {
        $data['error_MCC'] = $this->error['MCC'];
      } else {
        $data['error_MCC'] = '';
      }

      if (isset($this->error['CurrencyCode'])) {
        $data['error_CurrencyCode'] = $this->error['CurrencyCode'];
      } else {
        $data['error_CurrencyCode'] = '';
      }

      if (isset($this->error['TransactionType'])) {
        $data['error_TransactionType'] = $this->error['TransactionType'];
      } else {
        $data['error_TransactionType'] = '';
      }

      if (isset($this->error['SecureKey'])) {
        $data['error_SecureKey'] = $this->error['SecureKey'];
      } else {
        $data['error_SecureKey'] = '';
      }

      if (isset($this->error['EncryptionKey'])) {
        $data['error_EncryptionKey'] = $this->error['EncryptionKey'];
      } else {
        $data['error_EncryptionKey'] = '';
      }

      if (isset($this->error['GatewayProductionURL'])) {
        $data['error_GatewayProductionURL'] = $this->error['GatewayProductionURL'];
      } else {
        $data['error_GatewayProductionURL'] = '';
      }


      if (isset($this->error['GatewayTestURL'])) {
        $data['error_GatewayTestURL'] = $this->error['GatewayTestURL'];
      } else {
        $data['error_GatewayTestURL'] = '';
      }
        
        if (isset($this->error['ReturnURL'])) {
        $data['error_ReturnURL'] = $this->error['ReturnURL'];
      } else {
        $data['error_ReturnURL'] = '';
      }

      if (isset($this->error['StatusURL'])) {
        $data['error_StatusURL'] = $this->error['StatusURL'];
      } else {
        $data['error_StatusURL'] = '';
      }

      if (isset($this->error['RefundURL'])) {
        $data['error_RefundURL'] = $this->error['RefundURL'];
      } else {
        $data['error_RefundURL'] = '';
      }

     //end of merchant details 

      if (isset($this->error['merchant_type'])) {
        $data['error_merchant_type'] = $this->error['merchant_type'];
      } else {
        $data['error_merchant_type'] = '';
      }

      
    $data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text'      => $this->language->get('text_home'),
      'href'      =>  $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
      'separator' => false
    );

    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_extension'),
      'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
    );

    $data['breadcrumbs'][] = array(
      'text'      => $this->language->get('heading_title'),
      'href'      => $this->url->link('extension/payment/custompayment', 'user_token=' . $this->session->data['user_token'], true),
      'separator' => ' :: '
    );

    $data['action'] = $this->url->link('extension/payment/custompayment', 'user_token=' . $this->session->data['user_token'], true);

    $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token']  . '&type=payment', true);


    
      if (isset($this->request->post['payment_custompayment_Version'])) {
        $data['payment_custompayment_Version'] = $this->request->post['payment_custompayment_Version'];
      } else {
         $data['payment_custompayment_Version'] = $this->config->get('payment_custompayment_Version');
      }
    
      if (isset($this->request->post['payment_custompayment_AccessCode'])) {
            $data['payment_custompayment_AccessCode'] = $this->request->post['payment_custompayment_AccessCode'];
          } else {
            $data['payment_custompayment_AccessCode'] = $this->config->get('payment_custompayment_AccessCode');
          } 
      if (isset($this->request->post['payment_custompayment_BankId'])) {
            $data['payment_custompayment_BankId'] = $this->request->post['payment_custompayment_BankId'];
          } else {
            $data['payment_custompayment_BankId'] = $this->config->get('payment_custompayment_BankId');
          }
       if (isset($this->request->post['payment_custompayment_TerminalId'])) {
            $data['payment_custompayment_TerminalId'] = $this->request->post['payment_custompayment_TerminalId'];
          } else {
            $data['payment_custompayment_TerminalId'] = $this->config->get('payment_custompayment_TerminalId');
          }
       if (isset($this->request->post['payment_custompayment_MerchantID'])) {
            $data['payment_custompayment_MerchantID'] = $this->request->post['payment_custompayment_MerchantID'];
          } else {
            $data['payment_custompayment_MerchantID'] = $this->config->get('payment_custompayment_MerchantID');
          }
      if (isset($this->request->post['payment_custompayment_MCC'])) {
            $data['payment_custompayment_MCC'] = $this->request->post['payment_custompayment_MCC'];
          } else {
            $data['payment_custompayment_MCC'] = $this->config->get('payment_custompayment_MCC');
          }
      if (isset($this->request->post['payment_custompayment_CurrencyCode'])) {
            $data['payment_custompayment_CurrencyCode'] = $this->request->post['payment_custompayment_CurrencyCode'];
          } else {
            $data['payment_custompayment_CurrencyCode'] = $this->config->get('payment_custompayment_CurrencyCode');
          }
      if (isset($this->request->post['payment_custompayment_TransactionType'])) {
            $data['payment_custompayment_TransactionType'] = $this->request->post['payment_custompayment_TransactionType'];
          } else {
            $data['payment_custompayment_TransactionType'] = $this->config->get('payment_custompayment_TransactionType');
          }   
      if (isset($this->request->post['payment_custompayment_SecureKey'])) {
            $data['payment_custompayment_SecureKey'] = $this->request->post['payment_custompayment_SecureKey'];
          } else {
            $data['payment_custompayment_SecureKey'] = $this->config->get('payment_custompayment_SecureKey');
          }
      if (isset($this->request->post['payment_custompayment_EncryptionKey'])) {
            $data['payment_custompayment_EncryptionKey'] = $this->request->post['payment_custompayment_EncryptionKey'];
          } else {
            $data['payment_custompayment_EncryptionKey'] = $this->config->get('payment_custompayment_EncryptionKey');
          }
      if (isset($this->request->post['payment_custompayment_GatewayProductionURL'])) {
            $data['payment_custompayment_GatewayProductionURL'] = $this->request->post['payment_custompayment_GatewayProductionURL'];
          } else {
            $data['payment_custompayment_GatewayProductionURL'] = $this->config->get('payment_custompayment_GatewayProductionURL');
          }

      if (isset($this->request->post['payment_custompayment_GatewayTestURL'])) {
            $data['payment_custompayment_GatewayTestURL'] = $this->request->post['payment_custompayment_GatewayTestURL'];
          } else {
            $data['payment_custompayment_GatewayTestURL'] = $this->config->get('payment_custompayment_GatewayTestURL');
          }


      if (isset($this->request->post['payment_custompayment_ReturnURL'])) {
            $data['payment_custompayment_ReturnURL'] = $this->request->post['payment_custompayment_ReturnURL'];
          } else {
            $data['payment_custompayment_ReturnURL'] = $this->config->get('payment_custompayment_ReturnURL');
          }

       if (isset($this->request->post['payment_custompayment_StatusURL'])) {
            $data['payment_custompayment_StatusURL'] = $this->request->post['payment_custompayment_StatusURL'];
          } else {
            $data['payment_custompayment_StatusURL'] = $this->config->get('payment_custompayment_StatusURL');
          } 
          
      if (isset($this->request->post['payment_custompayment_RefundURL'])) {
            $data['payment_custompayment_RefundURL'] = $this->request->post['payment_custompayment_RefundURL'];
          } else {
            $data['payment_custompayment_RefundURL'] = $this->config->get('payment_custompayment_RefundURL');
          } 


  		if (isset($this->request->post['payment_custompayment_merchant_type'])) {
  			$data['payment_custompayment_merchant_type'] = $this->request->post['payment_custompayment_merchant_type'];
  		} else {
  			$data['payment_custompayment_merchant_type'] = $this->config->get('payment_custompayment_merchant_type');
  		}

		
      if (isset($this->request->post['payment_custompayment_completed_status_id'])) {
        $data['payment_custompayment_completed_status_id'] = $this->request->post['payment_custompayment_completed_status_id'];
      } else {
        $data['payment_custompayment_completed_status_id'] = $this->config->get('payment_custompayment_completed_status_id');
      }

      if (isset($this->request->post['payment_custompayment_failed_status_id'])) {
        $data['payment_custompayment_failed_status_id'] = $this->request->post['payment_custompayment_failed_status_id'];
      } else {
        $data['payment_custompayment_failed_status_id'] = $this->config->get('payment_custompayment_failed_status_id');
      }

      $this->load->model('localisation/order_status');

    $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

    if (isset($this->request->post['payment_custompayment_status'])) {
      $data['payment_custompayment_status'] = $this->request->post['payment_custompayment_status'];
    } else {
      $data['payment_custompayment_status'] = $this->config->get('payment_custompayment_status');
    }

     $this->load->model('localisation/geo_zone');
   
    $data['user_token'] = $this->session->data['user_token'];

    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('extension/payment/custompayment', $data));
  }

  private function validate() {
    if (!$this->user->hasPermission('modify', 'extension/payment/custompayment')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }

    //validation errors
     // Version validation
      if (!$this->request->post['payment_custompayment_Version']) {
        $this->error['Version'] = $this->language->get('error_Version');
      }elseif (!is_numeric($this->request->post['payment_custompayment_Version'])) {
        $this->error['Version'] = $this->language->get('error_Version_valid');
      }else if(strlen($this->request->post['payment_custompayment_Version']) != 1) {
      $this->error['Version']=$this->language->get('error_Version_valid_length');
      }else{

      }
     // AccessCode validation 
     if (!$this->request->post['payment_custompayment_AccessCode']) {
          $this->error['AccessCode'] = $this->language->get('error_AccessCode');
        }elseif(!ctype_alnum($this->request->post['payment_custompayment_AccessCode'])){
          $this->error['AccessCode'] = $this->language->get('error_AccessCode_valid');
        }elseif(strlen($this->request->post['payment_custompayment_AccessCode']) != 8){
          $this->error['AccessCode']=$this->language->get('error_AccessCode_valid_length');
        }else{

        } 
     //BankId valiation
     if (!$this->request->post['payment_custompayment_BankId']) {
          $this->error['BankId'] = $this->language->get('error_BankId');
        }elseif(!ctype_alnum($this->request->post['payment_custompayment_BankId'])){
          $this->error['BankId'] = $this->language->get('error_BankId_valid');
        }elseif(strlen($this->request->post['payment_custompayment_BankId']) != 6){
          $this->error['BankId']=$this->language->get('error_BankId_valid_length');
        }else{

        } 

      //TerminalId validation
     if (!$this->request->post['payment_custompayment_TerminalId']) {
          $this->error['TerminalId'] = $this->language->get('error_TerminalId');
        }elseif(!ctype_alnum($this->request->post['payment_custompayment_TerminalId'])){
          $this->error['TerminalId'] = $this->language->get('error_TerminalId_valid');
        }elseif(strlen($this->request->post['payment_custompayment_TerminalId']) != 8){
          $this->error['TerminalId']=$this->language->get('error_TerminalId_valid_length');
        }else{

        } 

      //MerchantID validation
      if (!$this->request->post['payment_custompayment_MerchantID']) {
            $this->error['MerchantID'] = $this->language->get('error_MerchantID');
          }elseif(!ctype_alnum($this->request->post['payment_custompayment_MerchantID'])){
          $this->error['MerchantID'] = $this->language->get('error_MerchantID_valid');
        }elseif(strlen($this->request->post['payment_custompayment_MerchantID']) != 15){
          $this->error['MerchantID']=$this->language->get('error_MerchantID_valid_length');
        }else{

        } 
      //Merchant Category Code(MCC) validation

      if (!$this->request->post['payment_custompayment_MCC']) {
            $this->error['MCC'] = $this->language->get('error_MCC');
          }elseif(!is_numeric($this->request->post['payment_custompayment_MCC'])){
          $this->error['MCC'] = $this->language->get('error_MCC_valid');
        }elseif(strlen($this->request->post['payment_custompayment_MCC']) != 4){
          $this->error['MCC']=$this->language->get('error_MCC_valid_length');
        }else{

        } 

      // CurrencyCode validation
      if (!$this->request->post['payment_custompayment_CurrencyCode']) {
            $this->error['CurrencyCode'] = $this->language->get('error_CurrencyCode');
        }elseif(!is_numeric($this->request->post['payment_custompayment_CurrencyCode'])){
          $this->error['CurrencyCode'] = $this->language->get('error_CurrencyCode_valid');
        }elseif(strlen($this->request->post['payment_custompayment_CurrencyCode']) != 3){
          $this->error['CurrencyCode']=$this->language->get('error_CurrencyCode_valid_length');
        }else{

        } 


      // TransactionType validation
      if (!$this->request->post['payment_custompayment_TransactionType']) {
            $this->error['TransactionType'] = $this->language->get('error_TransactionType');
          }elseif(!ctype_alpha($this->request->post['payment_custompayment_TransactionType'])){
          $this->error['TransactionType'] = $this->language->get('error_TransactionType_valid');
        }elseif(strlen($this->request->post['payment_custompayment_TransactionType']) != 3 && strlen($this->request->post['payment_custompayment_TransactionType']) != 7){
          $this->error['TransactionType']=$this->language->get('error_TransactionType_valid_length');
        }else{

        } 
 
      // SecureKey validation
      if (!$this->request->post['payment_custompayment_SecureKey']) {
            $this->error['SecureKey'] = $this->language->get('error_SecureKey');
          }elseif(!ctype_alnum($this->request->post['payment_custompayment_SecureKey'])){
          $this->error['SecureKey'] = $this->language->get('error_SecureKey_valid');
        }


      // EncryptionKey validation
      if (!$this->request->post['payment_custompayment_EncryptionKey']) {
            $this->error['EncryptionKey'] = $this->language->get('error_EncryptionKey');
          }elseif(!ctype_alnum($this->request->post['payment_custompayment_EncryptionKey'])){
          $this->error['EncryptionKey'] = $this->language->get('error_EncryptionKey_valid');
        }


      // if (!$this->request->post['payment_custompayment_GatewayProductionURL']) {
      //       $this->error['GatewayProductionURL'] = $this->language->get('error_GatewayProductionURL');
      //     }
      // if (!$this->request->post['payment_custompayment_GatewayTestURL']) {
      //       $this->error['GatewayTestURL'] = $this->language->get('error_GatewayTestURL');
      //     } 

        //ReturnURL validation

      if (!$this->request->post['payment_custompayment_ReturnURL']) {
            $this->error['ReturnURL'] = $this->language->get('error_ReturnURL');
          }elseif(filter_var($this->request->post['payment_custompayment_ReturnURL'], FILTER_VALIDATE_URL) === FALSE){
          $this->error['ReturnURL'] = $this->language->get('error_ReturnURL_valid');
        }elseif(strlen($this->request->post['payment_custompayment_ReturnURL']) < 1){
          $this->error['ReturnURL']=$this->language->get('error_ReturnURL_valid_length');
        }elseif(strlen($this->request->post['payment_custompayment_ReturnURL']) > 225){
          $this->error['ReturnURL']=$this->language->get('error_ReturnURL_valid_length');
        }else{

        }
  
      // if (!$this->request->post['payment_custompayment_StatusURL']) {
      //       $this->error['StatusURL'] = $this->language->get('error_StatusURL');
      //     } 
      // if (!$this->request->post['payment_custompayment_RefundURL']) {
      //       $this->error['RefundURL'] = $this->language->get('error_RefundURL');
      //     } 


		if (!isset($this->request->post['payment_custompayment_merchant_type'])) {
			$this->error['merchant_type'] = $this->language->get('error_merchant_type');
		} else {
      $sum = 0;
      foreach($this->request->post['payment_custompayment_merchant_type'] as $k => $v) {
        $sum = $sum + $this->request->post['payment_custompayment_merchant_type'][$k];
      }
      if ($sum == 0)
  			$this->error['merchant_type'] = $this->language->get('error_merchant_type');
    }

    return !$this->error;
  }
}
  