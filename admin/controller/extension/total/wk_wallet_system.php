<?php
class ControllerExtensionTotalWkwalletsystem extends Controller {
	private $error = array();

	public function install() {
		$this->load->model('wallet/wk_wallet');
		$this->model_wallet_wk_wallet->createTable();
		$this->load->model('setting/extension');
		$this->model_setting_extension->install('payment', 'wk_wallet_system_payment');

		$languages = $this->model_wallet_wk_wallet->getLanguages();

		$description_array = array();

		foreach ($languages as $language) {
			$description_array[$language['language_id']] = array(
				'name'             => 'Wallet',
				'description'      => 'Wallet',
				'tag'              => 'Wallet',
				'meta_title'       => 'Wallet',
				'meta_description' => 'Wallet',
				'meta_keyword'     => 'Wallet'
				);
		}

		$store_array[] = 0;
		$this->load->model('setting/store');
		$stores = $this->model_setting_store->getStores();

		foreach ($stores as $store) {
			$store_array[] = $store['store_id'];
		}

		$this->load->model('catalog/product');

		$data = array(
			'model'               => 'Wallet',
			'sku'                 => '',
			'upc'                 => '',
			'ean'                 => '',
			'jan'                 => '',
			'isbn'                => '',
			'mpn'                 => '',
			'location'            => '',
			'quantity'            => '1',
			'minimum'             => '1',
			'subtract'            => '0',
			'stock_status_id'     => '',
			'date_available'      => '',
			'manufacturer_id'     => '',
			'shipping'            => '0',
			'price'               => '0',
			'points'              => '',
			'weight'              => '',
			'weight_class_id'     => '',
			'length'              => '',
			'width'               => '',
			'height'              => '',
			'length_class_id'     => '',
			'status'              => '1',
			'tax_class_id'        => '',
			'sort_order'          => '0',
			'product_description' => $description_array,
			'product_store'       => $store_array
		);

		$wallet['wallet_product_id'] = $this->model_catalog_product->addProduct($data);

		$this->load->model('setting/setting');
		$this->model_setting_setting->editSetting('wallet_product', $wallet);
	}

	public function uninstall() {
		$this->load->model('wallet/wk_wallet');
		$this->model_wallet_wk_wallet->deleteTable();
		$this->load->model('setting/extension');
		$this->model_setting_extension->uninstall('payment', 'wk_wallet_system_payment');
		$this->load->model('catalog/product');
		$this->model_catalog_product->deleteProduct($this->config->get('wallet_product_id'));
		$this->load->model('setting/setting');
		$this->model_setting_setting->deleteSetting('wallet_product');
	}

	public function index() {
		$data = $this->load->language('extension/total/wk_wallet_system');

		$this->document->setTitle($this->language->get('heading_title1'));
		$this->load->model('setting/setting');
		$this->load->model('wallet/wk_wallet');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (isset($this->request->post['wk_wallet_system_wallet_image'])) {
				$this->model_wallet_wk_wallet->updateProductImage($this->config->get('wallet_product_id'), $this->request->post['wk_wallet_system_wallet_image']);
			}

			$this->model_setting_setting->editSetting('total_wk_wallet_system', $this->request->post);
			$this->model_setting_setting->editSetting('wk_wallet_system', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/total/wk_wallet_system', 'user_token=' . $this->session->data['user_token'], true));
		}

		$error_array = array(
			'total_name',
			'payment_name',
			'min_refund',
			'max_refund'
			);

		foreach ($error_array as $error_val) {
			if (isset($this->error[$error_val])) {
				$data['error_'.$error_val] = $this->error[$error_val];
			} else {
				$data['error_'.$error_val] = '';
			}
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_total'),
			'href' => $this->url->link('marketplace/extension', 'type=total&user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/total/wk_wallet_system', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/total/wk_wallet_system', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'type=total&user_token=' . $this->session->data['user_token'], true);

		$config_array = array(
					'min_recharge',
					'max_recharge',
					'min_refund',
					'max_refund',
					'sort_order',
					'order_status_id',
					'payment_status_id',
					'total_name',
					'payment_name',
					'terms',
					'register_credit',
					'low_balance_status',
					'low_bal_amount',
					'wallet_image',
					'transact_limit',
					'amount_limit',
					'min_transact',
					'max_transact',
					'transaction_status'
			);

		foreach ($config_array as $config_val) {
			if (isset($this->request->post['wk_wallet_system_' . $config_val])) {
				$data[$config_val] = $this->request->post['wk_wallet_system_' . $config_val];
			} else {
				$data[$config_val] = $this->config->get('wk_wallet_system_' . $config_val);
			}
		}

		if (isset($this->request->post['total_wk_wallet_system_status'])) {
			$data['status'] = $this->request->post['total_wk_wallet_system_status'];
		} else {
			$data['status'] = $this->config->get('total_wk_wallet_system_status');
		}

		if (isset($this->request->post['total_wk_wallet_system_sort_order'])) {
			$data['sort_order'] = $this->request->post['total_wk_wallet_system_sort_order'];
		} else {
			$data['sort_order'] = $this->config->get('total_wk_wallet_system_sort_order');
		}

		$this->load->model('tool/image');

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		if ($data['wallet_image']) {
			$data['wallet_image_placeholder'] = $this->model_tool_image->resize($data['wallet_image'], 100, 100);
		} else {
			$data['wallet_image_placeholder'] = $data['placeholder'];
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$this->load->model('customer/customer_group');

		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/total/wk_wallet_system', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/total/wk_wallet_system')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['wk_wallet_system_total_name']) {
			$this->error['total_name'] = $this->language->get('error_total_name');
		}

		if (!$this->request->post['wk_wallet_system_payment_name']) {
			$this->error['payment_name'] = $this->language->get('error_payment_name');
		}

		if (!$this->request->post['wk_wallet_system_min_refund']) {
			$this->error['min_refund'] = $this->language->get('error_min_refund');
		}

		if (!$this->request->post['wk_wallet_system_max_refund']) {
			$this->error['max_refund'] = $this->language->get('error_max_refund');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}
}
?>
