<?php

class ControllerMarketingNotifications extends Controller
{

	private $error = array();



	public function index()
	{

		$this->load->language('marketing/notifications');



		$this->document->setTitle($this->language->get('heading_title'));



		$data['user_token'] = $this->session->data['user_token'];



		$data['breadcrumbs'] = array();



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('text_home'),

			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)

		);



		$data['breadcrumbs'][] = array(

			'text' => $this->language->get('heading_title'),

			'href' => $this->url->link('marketing/notifications', 'user_token=' . $this->session->data['user_token'], true)

		);



		$data['cancel'] = $this->url->link('marketing/notifications', 'user_token=' . $this->session->data['user_token'], true);



		$this->load->model('setting/store');



		$data['stores'] = $this->model_setting_store->getStores();



		$this->load->model('customer/customer_group');



		$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();



		$data['header'] = $this->load->controller('common/header');

		$data['column_left'] = $this->load->controller('common/column_left');

		$data['footer'] = $this->load->controller('common/footer');



		$this->response->setOutput($this->load->view('marketing/notifications', $data));
	}



	public function send()
	{

		$this->load->language('marketing/notifications');



		$json = array();



		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			if (!$this->user->hasPermission('modify', 'marketing/notifications')) {

				$json['error']['warning'] = $this->language->get('error_permission');
			}



			if (!$this->request->post['subject']) {

				$json['error']['subject'] = $this->language->get('error_subject');
			}



			if (!$this->request->post['message']) {

				$json['error']['message'] = $this->language->get('error_message');
			}



			if (!$json) {

				$this->load->model('setting/store');



				$store_info = $this->model_setting_store->getStore($this->request->post['store_id']);



				if ($store_info) {

					$store_name = $store_info['name'];
				} else {

					$store_name = $this->config->get('config_name');
				}



				$this->load->model('setting/setting');

				$setting = $this->model_setting_setting->getSetting('config', $this->request->post['store_id']);

				$store_email = isset($setting['config_email']) ? $setting['config_email'] : $this->config->get('config_email');



				$this->load->model('customer/customer');



				$this->load->model('customer/customer_group');



				$this->load->model('sale/order');


				$email_total = 0;


				$fcmids = array();

				switch ($this->request->post['to']) {

					case 'newsletter':
					case 'customer_all':
					case 'customer_group':
						$fcmids['group'] = 'generel';
					break;

					case 'customer':
						break;

					case 'affiliate_all':
						break;

					case 'affiliate':
						break;

					case 'product':
						break;
				}

				if ($fcmids) {
					$json['result'] = $this->send_fcm_notification(null, $this->request->post['subject'], $this->request->post['message']);
					$json['success'] = "Successully sent.";
				} else {

					$json['error']['email'] = "fcmid not found!";
				}
			}
		}



		$this->response->addHeader('Content-Type: application/json');

		$this->response->setOutput(json_encode($json));
	}

	function send_fcm_notification($customer_fcmid, $title, $msg){
        require_once(DIR_SYSTEM . 'library/FCM/fcmNotification.php');
        $customer_fcmid = ($customer_fcmid)??'eUuA59LIRrY:APA91bFaShzrkXK6pP5koky2bKUCEQlV8N8BZ3e6gDkCsqPrPEMLSg6W7f6qkvDKVuS3VP3Jnrqb3cfSe6XepPdQJaDzaMWrIwOz6GJ1iOp_awLQaGzJthMlvUC0_Kph0E7i1nH4QXQI';
		$parms =[
			'title' => $title,
			'msg' => $msg
		];
		return fcmNotification::send($customer_fcmid, $parms);
    }
}
