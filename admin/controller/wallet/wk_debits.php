<?php
class ControllerWalletWkdebits extends Controller {

	public function index() {
		$data = $this->load->language('wallet/wk_wallet');

		$this->document->setTitle($this->language->get('heading_debit'));

		$this->load->model('wallet/wk_wallet');

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

		if (isset($this->request->get['filter_order_id'])) {
			$filter_order_id = $this->request->get['filter_order_id'];
		} else {
			$filter_order_id = null;
		}

		if (isset($this->request->get['filter_amount'])) {
			$filter_amount = $this->request->get['filter_amount'];
		} else {
			$filter_amount = null;
		}

		if (isset($this->request->get['filter_date_added'])) {
			$filter_date_added = $this->request->get['filter_date_added'];
		} else {
			$filter_date_added = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'date';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_amount'])) {
			$url .= '&filter_amount=' . $this->request->get['filter_amount'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_debit'),
			'href' => $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['debits'] = array();

		$filter_data = array(
			'filter_name'		=> $filter_name,
			'filter_email'		=> $filter_email,
			'filter_order_id'	=> $filter_order_id,
			'filter_amount'		=> $filter_amount,
			'filter_date_added'	=> $filter_date_added,
			'sort'				=> $sort,
			'order'				=> $order,
			'start'				=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'				=> $this->config->get('config_limit_admin')
		);

		$wk_debits_total = $this->model_wallet_wk_wallet->getTotalDebits($filter_data);

		$results = $this->model_wallet_wk_wallet->getDebits($filter_data);

		foreach ($results as $result) {
			if (empty($result['currency_code'])) {
				$result['currency_code'] = $this->config->get('config_currency');
			}
			if (!$result['order_id']) {
				$result['order_id'] = 'N/A';
			}
			$data['debits'][] = array(
				'name'       => $result['name'],
				'email'      => $result['email'],
				'order_id'   => $result['order_id'],
				'desc' 		 => $result['description'],
				'amount'     => $this->currency->format($result['amount'], $result['currency_code'], 1),
				'date' 		 => $result['date']
			);
		}

		$data['heading_title'] = $this->language->get('heading_debit');

		$data['text_list'] = $this->language->get('text_debit_list');

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_amount'])) {
			$url .= '&filter_amount=' . $this->request->get['filter_amount'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
		$data['sort_email'] = $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . '&sort=email' . $url, true);
		$data['sort_order_id'] = $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . '&sort=order_id' . $url, true);
		$data['sort_date'] = $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . '&sort=date' . $url, true);
		$data['sort_amount'] = $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . '&sort=amount' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . urlencode(html_entity_decode($this->request->get['filter_email'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}

		if (isset($this->request->get['filter_amount'])) {
			$url .= '&filter_amount=' . $this->request->get['filter_amount'];
		}

		if (isset($this->request->get['filter_date_added'])) {
			$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['user_token'] = $this->session->data['user_token'];
		$data['filter_name'] = $filter_name;
		$data['filter_email'] = $filter_email;
		$data['filter_order_id'] = $filter_order_id;
		$data['filter_amount'] = $filter_amount;
		$data['filter_date_added'] = $filter_date_added;

		$pagination = new Pagination();
		$pagination->total = $wk_debits_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('wallet/wk_debits', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($wk_debits_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($wk_debits_total - $this->config->get('config_limit_admin'))) ? $wk_debits_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $wk_debits_total, ceil($wk_debits_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('wallet/wk_debits_list', $data));
	}

	public function addDebit()	{
		$json = array();

		$this->load->language('wallet/wk_wallet');
		$this->load->model('wallet/wk_wallet');

		if (isset($this->request->post['customer_id']) && $this->request->post['customer_id']) {
			$data['customer_id'] = $this->request->post['customer_id'];
		} else {
			$json['customer'] = $this->language->get('error_customer');
		}
		if (isset($this->request->post['amount']) && $this->request->post['amount'] && is_numeric($this->request->post['amount']) && !($this->request->post['amount'] < 0)) {
			$data['amount'] = $this->request->post['amount'];
			if (isset($data['customer_id'])) {
				$wallet_balance = $this->model_wallet_wk_wallet->walletBalance($data['customer_id']);
				if ($data['amount'] > $wallet_balance) {
					$json['amount'] = sprintf($this->language->get('error_balance'), $this->currency->format($wallet_balance, $this->config->get('config_currency'), 1));
				}
			}
		} else {
			$json['amount'] = $this->language->get('error_amount');
		}
		if (isset($this->request->post['description']) && $this->request->post['description']) {
			$data['description'] = $this->request->post['description'];
		} else {
			$json['description'] = $this->language->get('error_description');
		}

		if (!$json) {
			$this->model_wallet_wk_wallet->addDebit($data);
			$json['success'] = $this->language->get('text_success_debit');
		} else {
			$json['error'] = $this->language->get('error_warning');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
