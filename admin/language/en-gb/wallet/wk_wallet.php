<?php
// Heading
$_['heading_credit']    = 'Credit History';
$_['heading_debit']     = 'Debit History';
$_['heading_customer']  = 'Customer Details';

// Column
$_['column_name']           = 'Customer Name';
$_['column_email']          = 'E-mail';
$_['column_amount']         = 'Amount';
$_['column_description']    = 'Description';
$_['column_order_id']       = 'Order ID';
$_['column_transaction_id'] = 'Transaction ID';
$_['column_amount']         = 'Amount';
$_['column_balance']        = 'Wallet balance';
$_['column_date']           = 'Date Added';
$_['column_bank_details']   = 'Bank Details';

// entry
$_['entry_name']                = 'Customer Name';
$_['entry_email']               = 'E-mail';
$_['entry_amount']              = 'Amount';
$_['entry_order_id']            = 'Order ID';
$_['entry_amount']              = 'Amount';
$_['entry_date']                = 'Date Added';
$_['entry_description']         = 'Description';
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';

// Text
$_['text_credit_list']    = 'Credit List';
$_['text_debit_list']     = 'Debit List';
$_['text_customer_list']  = 'Customer List';
$_['text_add_debit']      = 'Add Debit';
$_['text_add_credit']     = 'Add Credit';
$_['text_success_credit'] = 'Success: The credit has been successfully made.';
$_['text_success_debit']  = 'Success: The debit has been successfully made.';

// Mail
$_['text_transaction_received']    = 'A sum of %s have been successfully added to your wallet!';
$_['text_transaction_deduct']      = 'A sum of %s have been successfully deducted from your wallet!';
$_['text_current_balance']         = 'Your current wallet balance is %s';
$_['text_transaction_subject']     = '%s - Wallet System';
$_['text_transaction_description'] = 'Description: %s';
$_['text_dear']                    = 'Dear %s,';
$_['text_regards']                 = 'Regards';

// button
$_['button_filter']     = 'Filter';

// error
$_['error_customer']    = 'Please add a customer';
$_['error_amount']      = 'Please add an amount';
$_['error_description'] = 'Please add a description';
$_['error_balance']     = 'The wallet balance(%s) of this customer is lower than the debit amount';
$_['error_range']       = 'The refund amount must be between %s to %s';
$_['error_warning']     = 'Warning: Please check out the empty fields';
