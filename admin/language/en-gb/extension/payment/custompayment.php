<?php
// Heading
$_['heading_title']          = 'CustomPayment';
$_['text_edit']              = 'Edit configuration';

// Text
$_['text_extension']	    = 'Extensions';
$_['text_success']        = 'Success: You have modified payment module!';
$_['text_custompayment']      = '<img src="view/image/payment/custompayment.png" alt="" title="" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_order_status']                = 'Order Status Completed';
$_['entry_order_status_completed_text'] = 'Completed Status';
$_['entry_order_status_failed_text']    = 'Failed Status';
$_['entry_order_status_pending']        = 'Order Status Pending';
$_['entry_order_status_expiredate']     = 'Order Status Expiredate';
$_['entry_order_status_canceled']       = 'Order Status Canceled';
$_['entry_order_status_failed']         = 'Order Status Failed';
$_['entry_order_status_processing']     = 'Order Status Processing';

//module admin levels
 $_['entry_status']                  = 'Status';
 $_['entry_Version']                 = 'Version';
 $_['entry_AccessCode']        		 = 'Access Code';
 $_['entry_BankId']            		 = 'Bank Id';
 $_['entry_TerminalId']        		 = 'Terminal Id';
 $_['entry_MerchantID']        		 = 'Merchant ID';
 $_['entry_MCC']              		 = 'Merchant Category Code';
 $_['entry_CurrencyCode']     		 = 'Currency Code';
 $_['entry_TransactionType']         = 'Transaction Type';
 $_['entry_SecureKey']         		 = 'Secure Key';
 $_['entry_EncryptionKey']         	 = 'Encryption Key';
 $_['entry_GatewayProductionURL']    = 'Gateway Production URL';
 $_['entry_GatewayTestURL']          = 'Gateway Test URL';
 $_['entry_ReturnURL']         		 = 'Return URL';
 $_['entry_StatusURL']         	     = 'Status URL';
 $_['entry_RefundURL']         	     = 'Refund URL';

 
 //hint code
 $_['entry_Version_help'] 						= 'Version';
 $_['entry_AccessCode_help']        			= 'Access Code';
 $_['entry_BankId_help']            		 	= 'Bank Id';
 $_['entry_TerminalId_help']        		 	= 'Terminal Id';
 $_['entry_MerchantID_help']        		 	= 'Merchant ID';
 $_['entry_MCC_help']              		 		= 'Merchant Category Code';
 $_['entry_CurrencyCode_help']     				= 'Currency Code';
 $_['entry_TransactionType_help']         		= 'Transaction Type';
 $_['entry_SecureKey_help']         		 	= 'Secure Key';
 $_['entry_EncryptionKey_help']         	 	= 'Encryption Key';
 $_['entry_GatewayProductionURL_help']   		= 'Gateway Production URL';
 $_['entry_GatewayTestURL_help']   		        = 'Gateway Test URL';
 $_['entry_ReturnURL_help']         		 	= 'Return URL';
 $_['entry_StatusURL_help']         	     	= 'Status URL';
 $_['entry_RefundURL_help']         	     	= 'Refund URL';


// $_['entry_domain_payment_page']      = 'Payment page domain';
// $_['entry_domain_payment_page_help'] = 'Payment page domain received from your payment processor e.g. checkout.processor.com';




$_['entry_merchant_type']		            = 'Merchant Type';
$_['entry_merchant_type_twoparty']	    	= '2 Party';
$_['entry_merchant_type_threeparty']		= '3 Party';
//$_['entry_merchant_type_erip']		= 'ERIP';
//$_['entry_payment_custompayment_erip_service_no'] = 'ERIP service code';


// Error
$_['error_permission']      = 'Warning: You do not have permission to modify the payment module!';
 //$_['error_companyid']       = 'Shop Id required!';
 $_['error_Version']                = 'Version is required!';
 $_['error_AccessCode']             = 'Access Code is required!';
 $_['error_BankId']                 = 'Bank Id is required!';
 $_['error_TerminalId']             = 'Terminal Id is required!';
 $_['error_MerchantID']             = 'Merchant ID is required!';
 $_['error_MCC']                    = 'Merchant Category Code is required!';
 $_['error_CurrencyCode']           = 'Currency Code is required!';
 $_['error_TransactionType']        = 'Transaction Type is required!';
 $_['error_SecureKey']              = 'Secure Key is required!';
 $_['error_EncryptionKey']          = 'Encryption Key is required!';
 $_['error_GatewayProductionURL']   = 'Gateway Production URL is required!';
 $_['error_GatewayTestURL']         = 'Gateway Test URL is required!';
 $_['error_ReturnURL']              = 'Return URL is required!';
 $_['error_StatusURL']              = 'Status URL is required!';
 $_['error_RefundURL']              = 'Refund URL is required!';
 

//$_['error_encyptionkey']    = 'Shop secret key required!';
//$_['error_domain_payment_page']    = 'Payment page domain required!';
$_['error_merchant_type']		= 'At least one merchant type is required!';
//$_['error_erip_service_no'] = 'ERIP service code required!';


//error as per payment gateway
$_['error_Version_valid']           = 'Please Enter only Numeric';
$_['error_Version_valid_length']    = 'Please Enter only One Numeric';

$_['error_AccessCode_valid']        = 'Please Enter only Alphanumeric';
$_['error_AccessCode_valid_length'] = 'Please Enter only Eight Alphanumeric';

$_['error_BankId_valid']            = 'Please Enter only Alphanumeric';
$_['error_BankId_valid_length']     = 'Please Enter only Six Alphanumeric';

$_['error_TerminalId_valid']        = 'Please Enter only Alphanumeric';
$_['error_TerminalId_valid_length'] = 'Please Enter only Eight Alphanumeric';

$_['error_MerchantID_valid']         = 'Please Enter only Alphanumeric';
$_['error_MerchantID_valid_length']  = 'Please Enter only Fifteen Alphanumeric';

$_['error_MCC_valid']             	 = 'Please Enter only Numeric';
$_['error_MCC_valid_length']      	 = 'Please Enter only Four Numeric';

$_['error_CurrencyCode_valid']        = 'Please Enter only Numeric';
$_['error_CurrencyCode_valid_length'] = 'Please Enter only Three Numeric';

$_['error_CurrencyCode_valid']        = 'Please Enter only Numeric';
$_['error_CurrencyCode_valid_length'] = 'Please Enter only Three Numeric';

$_['error_TransactionType_valid']        = 'Please Enter only Alphabetic';
$_['error_TransactionType_valid_length'] = 'Please Enter only Three to Seven Alphabetic';

$_['error_ReturnURL_valid']        = 'Please Enter Valid Url';
$_['error_ReturnURL_valid_length'] = 'Please Enter Url length 1-225';

$_['error_SecureKey_valid']         = 'Please Enter only Alphanumeric';
$_['error_EncryptionKey_valid']         = 'Please Enter only Alphanumeric';
?>
