<?php
// Heading
$_['heading_title1']		= 'Wallet System';
$_['heading_title']			= '<b style="color:#414a51;">Wallet System</b>';

$_['text_total']    = 'Order Totals';
$_['text_success']  = 'Success: You have modified Wallet System module!';
$_['text_edit']     = 'Edit Wallet System Module';

// Entry
$_['entry_status']                 = 'Status';
$_['entry_min_recharge']           = 'Minimum Recharge Amount';
$_['entry_max_recharge']           = 'Maximum Recharge Amount';
$_['entry_min_refund']             = 'Minimum Refund Amount';
$_['entry_max_refund']             = 'Maximum Refund Amount';
$_['entry_order_status']           = 'Order Status';
$_['entry_payment_status']         = 'Payment Methods Order Status';
$_['entry_total_name']             = 'Order Total Name';
$_['entry_payment_name']           = 'Payment Method Name';
$_['entry_terms']	                 = 'Terms and Conditions';
$_['entry_sort_order']             = 'Sort Order';
$_['entry_wallet_image']           = 'Wallet Image';
$_['entry_low_bal_amount']         = 'Minimum Amount';
$_['entry_transaction_status']     = 'Apply Limit On Transfer';
$_['entry_transact_limit']         = 'Transfer Limit';
$_['entry_min_transact']           = 'Minimum Transfer Amount';
$_['entry_max_transact']           = 'Maximum Transfer Amount';
$_['entry_amount_limit']	         = 'Amount Limit';
$_['entry_group_credit']           = 'Enter wallet money credit for this group';

// Tags
$_['tag_recharge']	      = 'Recharge Limit';
$_['tag_refund']	        = 'Refund Limit';
$_['tag_total']		        = 'Manage Order Total';
$_['tag_sign_up_credit']  = 'Wallet Money Credit For Newly Registered Customers (Customer group-wise)';
$_['tag_low_balance']     = 'Low balance notification';
$_['tag_payment']	        = 'Manage Payment Method';

// Help
$_['help_min_recharge']	          = 'Specify the minimum recharge limit, otherwise there will be no lower limit';
$_['help_max_recharge']           = 'Specify the maximum recharge limit, otherwise there will be no upper limit';
$_['help_min_refund']		          = 'Specify the minimum refund limit, otherwise there will be no lower limit';
$_['help_max_refund']		          = 'Specify the maximum refund limit, otherwise there will be no upper limit';
$_['help_max_number_transaction'] = 'Specify the maximum number of transactions (valid for one month)';
$_['help_min_transaction_amount'] = 'Specify the minimum transaction amount';
$_['help_max_transaction_amount'] = 'Specify the maximum transaction amount';
$_['help_amount_limit']           = 'Specify the amount limit for overall transactions (valid for one month)';

// Tabs
$_['tab_general']     = 'General Configuration';
$_['tab_total']       = 'Order Total Settings';
$_['tab_payment']     = 'Payment Method Settings';
$_['tab_transaction'] = 'Transfer Settings';
$_['tab_terms']       = 'Terms and Conditions';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Wallet System module!';
$_['error_warning']	     = 'Warning: Please watch out the fields very carefully!';
$_['error_total_name']   = 'Warning: Please provide the name of order total';
$_['error_payment_name'] = 'Warning: Please provide the name of the wallet payment method';
$_['error_min_refund']   = 'Warning: Please provide the minimum refund amount';
$_['error_max_refund']   = 'Warning: Please provide the maximum refund amount';
