<?php
class ModelWalletWkwallet extends Model {
/**
 * Creates table for information regarding wallet system
 * @return null
 */
	public function createTable() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_wallet` (
			  `wallet_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `customer_id` INT(11) NOT NULL,
			  `order_id` INT(11) NOT NULL,
			  `type` CHAR(2) NOT NULL,
			  `date` DATETIME NOT NULL,
			  `amount` DECIMAL( 10, 2 ) NOT NULL,
			  `status` TINYINT (1) NOT NULL,
			  `zero_wallet` TINYINT (1) NOT NULL,
			  `description` VARCHAR(255) NOT NULL,
			  PRIMARY KEY (`wallet_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wallet_bank` (
			  `wallet_bank_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `customer_id` INT(11) NOT NULL,
			  `bank_name` VARCHAR(64) NOT NULL,
			  `bank_branch_number` VARCHAR(64) NOT NULL,
			  `bank_swift_code` VARCHAR(64) NOT NULL,
			  `bank_account_name` VARCHAR(64) NOT NULL,
			  `bank_account_number` VARCHAR(64) NOT NULL,
			  PRIMARY KEY (`wallet_bank_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_wallet_transaction` (
			  `transaction_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `sender_id` INT(11) NOT NULL,
			  `sender_email` VARCHAR(255) NOT NULL,
			  `receiver_id` INT(11) NOT NULL,
			  `receiver_email` VARCHAR(255) NOT NULL,
			  `amount` DECIMAL( 15, 2 ) NOT NULL,
			  `date` DATETIME NOT NULL,
			  `status` TINYINT (1) NOT NULL,
			  PRIMARY KEY (`transaction_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");

		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "wk_wallet_payee` (
			  `payee_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `customer_id` INT(11) NOT NULL,
			  `payee_email` VARCHAR(255) NOT NULL,
			  `date_added` DATETIME NOT NULL,
			  PRIMARY KEY (`payee_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");
	}
/**
 * Deletes table
 * @return null
 */
	public function deleteTable() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "wk_wallet`;");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "wallet_bank`;");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "wk_wallet_transaction`;");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "wk_wallet_payee`;");
	}
/**
 * Fetches credit history of customers
 * @param  array  $data contains data for sort and order
 * @return array       contains description of credits made
 */
	public function getCredits($data = array()) {
		$sql = "SELECT CONCAT(c.firstname, ' ', c.lastname) AS name, c.email, w.order_id, w.date, w.amount, w.description, o.currency_code FROM `" . DB_PREFIX . "wk_wallet` w LEFT JOIN `". DB_PREFIX ."customer` c ON (w.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "order o ON (o.order_id = w.order_id) WHERE w.type = 'C'";

		if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_email']) && $data['filter_email']) {
			$sql .= " AND c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}

		if (isset($data['filter_order_id']) && $data['filter_order_id']) {
			$sql .= " AND w.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (isset($data['filter_amount']) && $data['filter_amount']) {
			$sql .= " AND w.amount = '" . (float)$data['filter_amount'] . "'";
		}

		if (isset($data['filter_date_added']) && $data['filter_date_added']) {
			$sql .= " AND DATE(w.date) = 'DATE(" . $this->db->escape($data['filter_date_added']) . ")'";
		}

		$sort_data = array(
			'name',
			'email',
			'order_id',
			'date',
			'amount'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
/**
 * Fetches the total number of credits made
 * @return int contains total number of credits
 */
	public function getTotalCredits($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "wk_wallet` w LEFT JOIN `". DB_PREFIX ."customer` c ON (w.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "order o ON (o.order_id = w.order_id) WHERE w.type = 'C'";

		if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_email']) && $data['filter_email']) {
			$sql .= " AND c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}

		if (isset($data['filter_order_id']) && $data['filter_order_id']) {
			$sql .= " AND w.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (isset($data['filter_amount']) && $data['filter_amount']) {
			$sql .= " AND w.amount = '" . (float)$data['filter_amount'] . "'";
		}

		if (isset($data['filter_date_added']) && $data['filter_date_added']) {
			$sql .= " AND DATE(w.date) = 'DATE(" . $this->db->escape($data['filter_date_added']) . ")'";
		}

		$query = $this->db->query($sql);

		return $query->num_rows;
	}
/**
 * Fetches debit history of customers
 * @param  array  $data contains data for sort and order
 * @return array       contains description of debits made by customers
 */
	public function getDebits($data = array()) {
		$sql = "SELECT CONCAT(c.firstname, ' ', c.lastname) as `name`, c.email, w.order_id, w.date, w.amount, w.description, o.currency_code FROM `" . DB_PREFIX . "wk_wallet` w LEFT JOIN `". DB_PREFIX ."customer` c ON (w.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "order o ON (o.order_id = w.order_id) WHERE w.type = 'D'";

		if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_email']) && $data['filter_email']) {
			$sql .= " AND c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}

		if (isset($data['filter_order_id']) && $data['filter_order_id']) {
			$sql .= " AND w.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (isset($data['filter_amount']) && $data['filter_amount']) {
			$sql .= " AND w.amount = '" . (float)$data['filter_amount'] . "'";
		}

		if (isset($data['filter_date_added']) && $data['filter_date_added']) {
			$sql .= " AND DATE(w.date) = 'DATE(" . $this->db->escape($data['filter_date_added']) . ")'";
		}

		$sort_data = array(
			'name',
			'email',
			'order_id',
			'date',
			'amount'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
/**
 * Fetches the total number of debits made
 * @return int contains the total number of debits
 */
	public function getTotalDebits($data = array()) {
		$sql = "SELECT * FROM `" . DB_PREFIX . "wk_wallet` w LEFT JOIN `". DB_PREFIX ."customer` c ON (w.customer_id = c.customer_id) WHERE w.type = 'D'";

		if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_email']) && $data['filter_email']) {
			$sql .= " AND c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}

		if (isset($data['filter_order_id']) && $data['filter_order_id']) {
			$sql .= " AND w.order_id = '" . (int)$data['filter_order_id'] . "'";
		}

		if (isset($data['filter_amount']) && $data['filter_amount']) {
			$sql .= " AND w.amount = '" . (float)$data['filter_amount'] . "'";
		}

		if (isset($data['filter_date_added']) && $data['filter_date_added']) {
			$sql .= " AND DATE(w.date) = 'DATE(" . $this->db->escape($data['filter_date_added']) . ")'";
		}

		$query = $this->db->query($sql);

		return $query->num_rows;
	}
/**
 * adds a refund to customer wallet from back end
 * @param array $data contains details
 */
	public function addCredit($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "wk_wallet SET customer_id = '" . (int)$data['customer_id'] . "', type='C', amount = '" . (float)$data['amount'] . "', description = '" . $this->db->escape($data['description']) . "', date = NOW(), status = '1'";

		$this->db->query($sql);
		$this->sendMail($data);
	}
/**
 * adds a debit from customer's wallet from back end
 * @param array $data contains details
 */
	public function addDebit($data) {
		$sql = "INSERT INTO " . DB_PREFIX . "wk_wallet SET customer_id = '" . (int)$data['customer_id'] . "', type='D', amount = '" . (float)$data['amount'] . "', description = '" . $this->db->escape($data['description']) . "', date = NOW(), status = '1'";

		$this->db->query($sql);
		$this->sendDeductMail($data);
	}
/**
 * Fetches all the available language in the site
 * @return array details of all languages
 */
	public function getLanguages() {
		$languages = $this->db->query("SELECT * FROM " . DB_PREFIX . "language ")->rows;

		return $languages;
	}
/**
 * Updates the product image of the wallet product
 * @param  int $product_id contains the product id of the wallet product
 * @param  string $image      contains the path of image
 * @return null             none
 */
	public function updateProductImage($product_id, $image)	{
		$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($image) . "' WHERE product_id = '" . $product_id . "'");
	}
/**
 * Fetches the customers with wallet
 * @param  array  $data contains filter data
 * @return array       returns the customers with wallet
 */
	public function getCustomers($data = array()) {
		$sql = "SELECT DISTINCT CONCAT(c.firstname, ' ', c.lastname) as `name`, c.email, wb.*, w.customer_id FROM `" . DB_PREFIX . "wk_wallet` w LEFT JOIN `". DB_PREFIX ."customer` c ON (w.customer_id = c.customer_id) LEFT JOIN " . DB_PREFIX . "wallet_bank wb ON (wb.customer_id = w.customer_id) WHERE w.type = 'C'";

		if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_email']) && $data['filter_email']) {
			$sql .= " AND c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}

		$sort_data = array(
			'name',
			'email'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY date";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}
/**
 * Fetches the total number of customers containing wallet
 * @return int contains the total number of customers
 */
	public function getTotalCustomers($data = array()) {
		$sql = "SELECT DISTINCT CONCAT(c.firstname, ' ', c.lastname) as `name`, c.email FROM `" . DB_PREFIX . "wk_wallet` w LEFT JOIN `". DB_PREFIX ."customer` c ON (w.customer_id = c.customer_id) WHERE w.type = 'C'";

		if (isset($data['filter_name']) && $data['filter_name']) {
			$sql .= " AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_email']) && $data['filter_email']) {
			$sql .= " AND c.email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->num_rows;
	}
/**
 * Fetches customer's wallet balance
 * @return float contains wallet balance
 */
	public function walletBalance($customer_id)	{
		$credit_amount = 0;
		$debit_amount = 0;

		$total_credits = $this->db->query("SELECT ww.amount, o.currency_code, o.currency_value FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) where ww.type = 'C' AND ww.customer_id = '". $customer_id ."' AND status = '1' AND zero_wallet = '0'")->rows;

		foreach ($total_credits as $credit) {
			if (!$credit['currency_code']) {
				$credit['currency_code'] = $this->config->get('config_currency');
			}
			$credit_amount += $this->currency->convert($credit['amount'], $credit['currency_code'], $this->config->get('config_currency'));
		}

		$total_debits = $this->db->query("SELECT ww.amount, o.currency_code, o.currency_value FROM " . DB_PREFIX . "wk_wallet ww LEFT JOIN " . DB_PREFIX . "order o ON (ww.order_id = o.order_id) where ww.type = 'D' AND ww.customer_id = '". $customer_id ."' AND status = '1' AND zero_wallet = '0'")->rows;

		foreach ($total_debits as $debit) {
			if (!$debit['currency_code']) {
				$debit['currency_code'] = $this->config->get('config_currency');
			}
			$debit_amount += $this->currency->convert($debit['amount'], $debit['currency_code'], $this->config->get('config_currency'));
		}

		$balance = $credit_amount - $debit_amount;

		return round($balance,2);
	}
/**
 * sends an email on successful transfer to wallet
 * @param array $data contains details sent in custom variable
 */
	protected function sendMail($data = array()) {
		if ($data) {
			$customer_id = $data['customer_id'];
			$amount = $data['amount'];
			$currency_code = $this->session->data['currency'];

			$this->load->language('wallet/wk_wallet');
			$this->load->model('customer/customer');
			$customer_info = $this->model_customer_customer->getCustomer($customer_id);

			$message  = sprintf($this->language->get('text_dear'), $customer_info['firstname'] . ' ' . $customer_info['lastname']) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_received'), $this->currency->format($amount, $currency_code, 1)) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_description'), $data['description']) . "\n\n";
			$message .= sprintf($this->language->get('text_current_balance'), $this->currency->format($this->walletBalance($customer_id), $currency_code, 1)) . "\n\n";
			$url = new Url(HTTP_CATALOG, HTTPS_CATALOG);
			$message .= $url->link('account/wk_wallet_system', '', version_compare(VERSION, '2.2.0.0', '<') ? 'SSL' : true) . "\n\n";
			$message .= $this->language->get('text_regards') . "\n\n";
			$message .= html_entity_decode($this->config->get('config_name'));

			/**
			 * mail to customer
			 */

	 		$mail = new Mail($this->config->get('config_mail_engine'));
	 		$mail->parameter = $this->config->get('config_mail_parameter');
	 		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
	 		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
	 		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
	 		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
	 		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
			$mail->setText($message);
			$mail->send();
		}
	}
/**
 * sends an email on successful deduction to wallet
 * @param array $data contains details sent in custom variable
 */
	protected function sendDeductMail($data = array()) {
		if ($data) {
			$customer_id = $data['customer_id'];
			$amount = $data['amount'];
			$currency_code = $this->session->data['currency'];

			$this->load->language('wallet/wk_wallet');
			$this->load->model('customer/customer');
			$customer_info = $this->model_customer_customer->getCustomer($customer_id);

			$message  = sprintf($this->language->get('text_dear'), $customer_info['firstname'] . ' ' . $customer_info['lastname']) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_deduct'), $this->currency->format($amount, $currency_code, 1)) . "\n\n";
			$message .= sprintf($this->language->get('text_transaction_description'), $data['description']) . "\n\n";
			$message .= sprintf($this->language->get('text_current_balance'), $this->currency->format($this->walletBalance($customer_id), $currency_code, 1)) . "\n\n";
			$url = new Url(HTTP_CATALOG, HTTPS_CATALOG);
			$message .= $url->link('account/wk_wallet_system', '', version_compare(VERSION, '2.2.0.0', '<') ? 'SSL' : true) . "\n\n";
			$message .= $this->language->get('text_regards') . "\n\n";
			$message .= html_entity_decode($this->config->get('config_name'));

			/**
			 * mail to customer
			 */

	 		$mail = new Mail($this->config->get('config_mail_engine'));
	 		$mail->parameter = $this->config->get('config_mail_parameter');
	 		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
	 		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
	 		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
	 		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
	 		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($customer_info['email']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(sprintf($this->language->get('text_transaction_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')));
			$mail->setText($message);
			$mail->send();
		}
	}
}
