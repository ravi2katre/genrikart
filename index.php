<?php 
//echo get_ip(); exit;                                 //echo "Hhhhhh"; exit;
//echo phpinfo(); exit;
// Version
define('VERSION', '3.0.2.0');
define('IS_DEBUG', false);



// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');

require_once('debug_after_index.php');
 